# Welcome to ProtoCPU’s documentation!

ProtoCPU is a software CPU model under development to mirror 
[c-class](https://shakti.org.in/cclass.html) processor in gem5 simulator with 
cycle accuracy. The model currently supports single threaded RISCV baremetal 
full system (FS) simulations in [gem5](https://www.gem5.org/) without 
trap/fault handling. There is also a UART device model attached to the CPU 
model for communication purposes. It is coded in C++ and Python.

The current model is compatible with version 20.0.0.1 of gem5. Building the 
model from source is a resource heavy process, requiring atleast 16GB of RAM 
and few hours of build time.  

## The following steps can be used to run the model:

### Dependencies:
Please resolve the dependencies listed [here](https://www.gem5.org/documentation/general_docs/building)

### Steps:
`git clone --branch v20.0.0.1 https://gem5.googlesource.com/public/gem5`<br>
`cd gem5` <br>
`scons build/RISCV/gem5.opt -j$(nproc)`<br>
`cd ../`<br>
`git clone https://gitlab.com/shaktiproject/tools/core-models-gem5`<br>
`cd core-models-gem5`<br>
`python3 setup.py ../gem5`<br>

Now follow the instruction printed by `setup.py`

### If the gem5 repository alread exists:
`git clone https://gitlab.com/shaktiproject/tools/core-models-gem5`<br>
`cd core-models-gem5`<br>
`python3 setup.py <path_to_gem5_simulator>`<br>

### To run a simulation with gem5:
`cd gem5`<br>
`build/RISCV/gem5.opt -r --stdout=cool --debug-flags=<debug_flag1, debug_flag2> ./configs/example/riscv/baremetal_l2_uart.py --exe=<path_to_riscv_binary>`

The output/execution trace can be found at "./m5out/cool"

## The following debug flags are available with the model:
### Individual debug flags:
     CPU FLAGS:
     ProtoCPU
     ProtoFetchInfo, ProtoFetchStage, ProtoFDpipe
     ProtoDecodeInfo, ProtoDecodeStage, ProtoDEpipe
     ProtoExecuteInfo, ProtoExecuteStage, ProtoEMpipe
     ProtoMemaccInfo, ProtoMemaccStage, ProtoMWpipe
     ProtoWritebackInfo, ProtoWritebackStage
     ProtoOperandForwardInfo, ProtoOperandForwardStage
     ProtoIport
     ProtoMemWrite
     ProtoMemRead

     UART FLAGS:
     ProtoUARTInfo, ProtoUARTWrite, ProtoUARTRead
### Compounded debug flags:
     CPU FLAGS:
     ProtoFetch, ProtoDecode, ProtoExecute, ProtoMemacc, ProtoWriteback, ProtoOperandForward
     ProtoStages
     ProtoPipes

     UART_FLAGS:
     ProtoUART

## To compile a new binary:
This [repository](https://gitlab.com/shaktiproject/cores/benchmarks) details 
the necessary environment required for building riscv binaries for the model.

Alternatively, one can also use the following command for achieving the same:
<br>
`riscv64-unknown-elf-gcc -Wl,--section-start=.text=0x80001000 -nostartfiles 
-nostdlib <src.c> -o <elf>`

The simulation is end by writing 0x20000 to 0x2000c<br>
Data can be written to UART TX register by writing 8 bit values to the address 
offset by 4 from the base address of UART (0x11304 by default)

## Characteristic conditions of the CPU model
* Typical Five stage pipeline
* Multi line I-cache access is allowed
* Multi-line D-cache access is not allowed (Store/Load misaligned)
* Decode stage stores the half word from previous fetch and recombines it with half word in the next fetch incase of an uncompressed instruction.
* Address calculation and dcache request for memory referencing instruction takes place at ALU stage
* Operand forward unit detects RAW dependencies
* Dcache response is processed in memory access stage
* Write back stage marks the end of an instruction and register update by that instruction
* Fault/trap handling support is being implemented
* Branch Predictor yet to be implemented

Note1: RAW dependency is detected by operand forward unit and resolved by pipeline stall.
Note2: Though the CPU model supports caches, it has been temporarily disabled to support the UART device

Depending on the debug flags selected, the traces of the execution are printed to the stdout, which can be used of analyzing.

## Why model CPU in gem5 ?

GEM5 provides the ability to devlop a modular CPU model which
can support plug and play style functional models for cache,
memory controllers, GPU, Branch predictors etc… which are readily available 
within the gem5 software ecosystem. Being a opensource project, it has garnered
a lot of constructive activities in it’s codebase for the past several years.
Besides, the support for RISCV in gem5 has gained a lot of traction
[recently](https://www.mail-archive.com/search?a=1&l=gem5-dev%40gem5.org&haswords=riscv&x=0&y=0&from=&subject=&datewithin=1d&date=&notwords=&o=newest).

## Outreach
The project was selected to be a part of technical session at RISC-V global forum 2020. The presentation can be viewed [here](https://www.youtube.com/watch?v=aCXYjl9iaVk)