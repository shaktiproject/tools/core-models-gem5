#ifndef __DEV_PROTOUART_UART_CC__
#define __DEV_PROTOUART_UART_CC__

#include "dev/protouart/uart.hh"
#include "debug/ProtoUARTInfo.hh"
#include "debug/ProtoUARTWrite.hh"
#include "debug/ProtoUARTRead.hh"

Addr ProtoUART::size = 0x40;

ProtoUART::ProtoUART(const Params *p )
:BasicPioDevice(p,size),startAddr(p->pio_addr)
{
    DPRINTF(ProtoUARTInfo,"The uart is instantiated \n");
}

Tick
ProtoUART::read(PacketPtr pkt)
{
    DPRINTF(ProtoUARTInfo,"The uart should read at %#x\n",pkt->getAddr());
    Addr raddr = pkt->getAddr() - pioAddr;
    pkt->setRaw((uint8_t)0);
    switch (raddr) {
        case 0:
            DPRINTF(ProtoUARTRead,"UART reading Baud register\n");
            pkt->setRaw((uint16_t)statusReg);
            break;
        case 4:
            DPRINTF(ProtoUARTRead,"UART reading TX register\n");
            pkt->setRaw((uint8_t)txReg);
            break;
        case 8:
            DPRINTF(ProtoUARTRead,"UART reading RX register\n");
            pkt->setRaw((uint8_t)rxReg);
            break;
        case 0xc:
            DPRINTF(ProtoUARTRead,"UART reading Status register\n");
            pkt->setRaw((uint8_t)statusReg);
            break;
        default:
            DPRINTF(ProtoUARTInfo,"Unknown UART register\n");
            break;
    }
    pkt->makeAtomicResponse();
    return pioDelay;
}

Tick
ProtoUART::write(PacketPtr pkt)
{
    Addr waddr = pkt->getAddr() - pioAddr;
    DPRINTF(ProtoUARTInfo,"The uart should write at %#x\n",pkt->getAddr());
    switch (waddr) {
        case 0:
            DPRINTF(ProtoUARTWrite,"UART wrting Baud register\n");
            baudReg = pkt->getRaw<uint16_t>();
            break;
        case 4:
            txReg = pkt->getRaw<uint8_t>();
            DPRINTF(ProtoUARTWrite,"UART writing TX register : \'%c\' \n",
                    txReg);
            break;
        case 8:
            rxReg = pkt->getRaw<uint8_t>();
            DPRINTF(ProtoUARTWrite,"UART writing RX register : \'%c\' \n",
                    rxReg);
            break;
        case 0xc:
            statusReg = pkt->getRaw<uint8_t>();
            DPRINTF(ProtoUARTWrite,"UART writing Status register : \'%c\' \n"
                    ,statusReg);
            break;
        default:
            DPRINTF(ProtoUARTInfo,"Unknown UART register\n");
            break;
    }
    pkt->makeAtomicResponse();
    return pioDelay;
}

AddrRangeList
ProtoUART::getAddrRanges() const
{
    AddrRangeList ranges;
    ranges.push_back(RangeSize(startAddr, size));
    return ranges;
}

ProtoUART *
ProtoUARTParams::create()
{
    return new ProtoUART(this);
}

#endif
