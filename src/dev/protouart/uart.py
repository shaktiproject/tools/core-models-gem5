from m5.params import *
from m5.objects.Device import BasicPioDevice

class ProtoUART(BasicPioDevice):
    type = 'ProtoUART'
    cxx_header = 'dev/protouart/uart.hh'
