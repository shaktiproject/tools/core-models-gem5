#ifndef __DEV_PROTOUART_UART_HH__
#define __DEV_PROTOUART_UART_HH__

#include "dev/io_device.hh"
#include "params/ProtoUART.hh"

class ProtoUART: public BasicPioDevice
{
    static Addr size;
    Addr startAddr;

  private:
    // const Addr baud_reg_addr = 0;
    // const Addr TX_reg_addr = 4;
    // const Addr RX_reg_addr = 8;
    // const Addr status_reg_addr = 0xc;

    uint16_t baudReg = 0 ;
    uint8_t txReg = 0;
    uint8_t rxReg = 0;
    uint8_t statusReg = 3;

  public:
    ProtoUART(const Params *p );
    Tick read(PacketPtr);
    Tick write(PacketPtr);    
    AddrRangeList getAddrRanges() const override;
};

#endif
