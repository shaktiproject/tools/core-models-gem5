#ifndef __CPU_PROTOCPU_IPORT_HH__
#define __CPU_PROTOCPU_IPORT_HH__

#include "cpu/base.hh"
#include "debug/ProtoIport.hh"

// CPU interface : Icache port
template <class CPU>
class Iport : public MasterPort
{

  private:

    enum istatus
    {
        Retry,
        WaitResponse,
        Avail,
    };
    istatus iPortStatus;
    
    CPU *icpu;

  public:
    Iport(CPU *_icpu) : MasterPort(_icpu->name() + ".icache_port", _icpu),
                                iPortStatus(Avail),icpu(_icpu) 
    {
    }

    bool
    sendTimingReq(PacketPtr pkt)
    {
        assert(iPortStatus == Avail || iPortStatus == Retry);

        if(MasterPort::sendTimingReq(pkt)){
                iPortStatus = WaitResponse;
                return true;
        }else iPortStatus = Retry;
        
        return false;
    }     
        
  protected:

    bool
    recvTimingResp(PacketPtr pkt) override 
    {                     
        assert(iPortStatus == WaitResponse);
        iPortStatus = Avail; 

        DPRINTF(ProtoIport,"Fetch: Received response from icache. \n");
        icpu->iFetch_recv_inst(pkt);
        return true;
    }
    
    void
    recvReqRetry() override // cache should not call this unnecessarily
    { 
        assert(iPortStatus == Retry);

        DPRINTF(ProtoIport,"Fetch: Icache free to take request. \n");
        icpu -> iFetch_resend_ireq();
        //resend the unsent packet
    }
};

#endif