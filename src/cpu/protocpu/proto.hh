#ifndef __CPU_PROTOCPU_PROTO_HH__
#define __CPU_PROTOCPU_PROTO_HH__

#include "cpu/protocpu/proto_hh.h"
#include "cpu/protocpu/iport.hh"
#include "cpu/protocpu/inst_translate.hh"
#include "cpu/protocpu/pipe.hh"
#include "cpu/protocpu/stage_buffer.hh"

// class FDpipe_buffer;
class InstFetch;
class InstDecode;
class InstExecute;
class InstMemacc;
class InstWriteback;
class OperandForward;

class ProtoContext; //Forward Declaration
class ProtoCPU : public BaseCPU
{
  public:
    std::vector<SimpleThread*> threadQueue;
    std::vector<ProtoContext*> ctxtQueue;
    std::list<ThreadID> activeThreads;
    ThreadID curThread;
    int mulStages, divStages;

  private:
    ProtoContext *threadCtxt;
    SimpleThread *thread;  
    typedef TheISA::MachInst MachInst; 
    static const MachInst noInst = -1;
    PacketPtr dcachePkt;
    MachInst buffered_inst = noInst;

  protected:
    BPredUnit *branchPred;
    enum cstatus
    {
        Idle=9,
        Running,
        Faulting,
    };
    cstatus cpuStatus;

  public:
    // ThreadID numThreads;
    // System *system;
    enum dstatus
    {
        DTLBWaitResponse=5,
        DcacheRetry,
        DcacheWaitResponse,
        DcacheIdle,
    };
    dstatus dPortStatus;
    Trace::InstRecord *traceData;
    TheISA::MachInst inst = noInst;
    
  public:
    ProtoCPU(ProtoCPUParams *prmtr);
    
    void init() override;
    /*!
        @note   only override if BaseCPU::init() and additional
                functions are added. 
                No need to override if it just contains BaseCPU::init()
     */
    void startup();
    void opFwdPushEvent();
    virtual ~ProtoCPU(); // Destructor has no custom functions inside
    void regStats(){
        BaseCPU::regStats();
    }

  private:
    void checkPcEventQueue();
    class SplitMainSenderState : public Packet::SenderState
    {
      public:
        int outstanding;
        PacketPtr fragments[2];

        int
        getPendingFragment()
        {
            if (fragments[0]) {
                return 0;
            } else if (fragments[1]) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    class SplitFragmentSenderState : public Packet::SenderState
    {
      public:
        SplitFragmentSenderState(PacketPtr _bigPkt, int _index) :
            bigPkt(_bigPkt), index(_index)
        {}
        PacketPtr bigPkt;
        int index;

        void
        clearFromParent()
        {
            SplitMainSenderState * main_send_state =
                dynamic_cast<SplitMainSenderState *>(bigPkt->senderState);
            main_send_state->fragments[index] = NULL;
        }
    };
    
  public:

    // This section deals with the Icache and Dcache interface for ProtoCPU
    Pipe<OFpipe_buffer> ofPipe;
    OperandForward *opfwdOne;

    Iport<ProtoCPU> iport;
    InstTranslate<ProtoCPU> instTranslate;
    MasterPort &getInstPort() override { return iport; }
    Pipe<FDpipe_buffer> fdPipe;
    InstFetch *fetchOne;

    Pipe<DEpipe_buffer> dePipe;
    InstDecode *decodeOne;
    
    Pipe<EMpipe_buffer> emPipe;
    InstExecute *executeOne;

    Pipe<MWpipe_buffer> mwPipe;
    InstMemacc *memaccOne;

    InstWriteback *writebackOne;

    // CPU interface : Dcache port
    class Dport : public MasterPort
    {
      protected:
        ProtoCPU *dcpu;

      public:
        Addr cacheBlockMask; 
        Dport(ProtoCPU *_dcpu) : MasterPort(_dcpu->name() + ".dcache_port", 
                                            _dcpu), dcpu(_dcpu),
                                            DtickEve(_dcpu)
        {
            cacheBlockMask = ~(dcpu->cacheLineSize() - 1);
        }

      protected:
        
        void recvTimingSnoopReq(PacketPtr pkt);

        void recvFunctionalSnoop(PacketPtr pkt);

        virtual bool 
        isSnooping() const 
        {
            return true;
        }

        bool recvTimingResp(PacketPtr pkt) ;
        void recvReqRetry();
        
        struct TickEvent : public Event
        {
            PacketPtr pkt;
            ProtoCPU *tcpu;

            TickEvent(ProtoCPU *_cpu) : pkt(NULL), tcpu(_cpu) {}
            const char 
            *description() const 
            { 
                return "Timing CPU dcache tick"; 
            }

            //Transfer the packet and begin schedule.
            void
            schedule(PacketPtr _pkt, Tick t)
            {
                pkt = _pkt;
                tcpu->schedule(this, t);
            }

            //Event to run on schedule.
            void
            process()
            {
                tcpu->completeDcacheAccess(pkt);
                pkt = NULL;
            }
        };
        TickEvent DtickEve;
    };
    Dport dport;
    MasterPort &getDataPort() override { return dport; }

    void
    wakeup(ThreadID tid) override 
    { 
        fatal("wakeup not implemented"); 
    }
    void activateContext(ThreadID thread_num) override; //defined
    void suspendContext() {}                            // override;
    void haltContext() {}                               // override;
    void switchOut() {}                                 // override;
    void takeOverFrom() {}                              // override;
    void verifyMemoryMode() const override;
    void serializeThread(CheckpointOut &cp, ThreadID tid) {} // const override;
    void unserializeThread(CheckpointIn &cp, ThreadID tid) {} // override;

    Counter
    totalInsts() const
    {
        Counter a = 0;
        return a;
    } 

    Counter
    totalOps() const
    {
        Counter a = 0;
        return a;
    }                                                           
    
    void probeInstCommit(const StaticInstPtr &inst, Addr pc) {}

    // Execution Context requirements
    //All taken care in the exec_context.hh
    void threadSnoop(PacketPtr pkt, ThreadID sender);

  public:
    void iFetch_send_ireq(const RequestPtr &, ThreadContext *);
    void iFetch_resend_ireq();
    void iFetch_recv_inst(PacketPtr pkt);

    Fault initiateMemRead(Addr addr, unsigned size, Request::Flags flags,
                     const std::vector<bool>& byteEnable=std::vector<bool>());
    Fault writeMem(uint8_t *data, unsigned size, Addr addr, 
                   Request::Flags flags, uint64_t *res,
                   const std::vector<bool>& byteEnable = std::vector<bool>());
    void finishTranslation(WholeTranslationState *state);
    /** This function is used by the page table walker to determine if it could
     * translate the a pending request or if the underlying request has been
     * squashed. 
    */  
    PacketPtr
    buildPacket(const RequestPtr &req, bool read)
    {
        return read ? Packet::createRead(req) : Packet::createWrite(req);
    }

    bool isSquashed() const { return false; }
    void sendDcacheReq(const RequestPtr &req, uint8_t *data, uint64_t *res,
                       bool read);
    bool sendDcacheReadReq(PacketPtr);
    bool sendDcacheWriteReq(PacketPtr pkt, uint64_t *res = NULL);
    Fault initiateMemAMO(Addr, unsigned, Request::Flags, AtomicOpFunctorPtr);
    void resendDcacheReq();
    void completeDcacheAccess(PacketPtr);
};

/*!
    @class  ProtoCPU
    @brief  ProtoCPU class template with some nested classes, objects, 
            uninitialized attributes/pointers, functions and enums.
            Undefined Functions are defined in other relevant hh,cc files.

    @bug    Wondering how the circular dependencies resolved with forward
            declaration of ProtoContext. Are ther any more impending 
            danger ?

 */
#endif