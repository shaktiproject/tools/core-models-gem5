#ifndef __CPU_PROTOCPU_INST_EXECUTE_CC__
#define __CPU_PROTOCPU_INST_EXECUTE_CC__

#include "cpu/protocpu/inst_execute.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

// Initializing the global kinda var
int InstExecute::executeStagesCount = 0;

void 
InstExecute::NegEdgeEvent::schedule()
{
    assert(!this->scheduled());
    ieu->cpu->schedule(this, curTick()+1);
}

void 
InstExecute::PosEdgeEvent::schedule(Tick t)
{
    assert(!this->scheduled());
    ieu->cpu->schedule(this, t);
}

int 
InstExecute::executeID()
{
    return _executeID;
}

void 
InstExecute::start()
{
    switchTC();
    executeEve.schedule(cpu->clockEdge());
}

void 
InstExecute::stop()
{
    if (executeEve.scheduled()) {
        cpu->deschedule(executeEve);
    }
    if (pipeEve.scheduled()) {
        cpu->deschedule(pipeEve);
    }
}

void 
InstExecute::feedbackCheck()
{
    // I think its time for all the memory instruction to just have aluPushlock
    // as the operand_forwarder takes care of the rest.
    if (isAluPushLocked() && emPipe.doneMemLoad()) {
        aluUnlock(); 
        aluPushUnlock();
    }
    if (isAluPushLocked() && emPipe.doneMemStore()) {
        aluPushUnlock(); 
        aluUnlock();
    }
    // This necessary to not execute the successive memref instruction when the
    // previous memref has not completed
    if (isAluPushLocked() && pulled && inputBuffer.data()->isMemRef()) {
        aluLock();
    }

}

void 
InstExecute::initCheck()
{
    DPRINTF(ProtoEMpipe,"Cycle: %u ALU : %d filled in EM pipe\n",
            uint64_t(cpu->curCycle()), emPipe.quantity());

    // If the previously popped instruction has begun executing in the 
    // past(usually in the previous cycle), pop new instruction.
    if (inputBuffer.touched() == true) pulled = dePipe.pop(&inputBuffer);

    // Check for feedbacks and release the appropriate locks on ALU
    feedbackCheck();

    // If ALU is available and we have new instruction in input_buffer and the 
    // previous instruction is pushed to emPipe and there is no lock on ALU 
    // execution 
    // Send it for operand forward check   
    if (stageState() == Avail && !inputBuffer.touched() && 
        internalBuffer.touched() == true && !isAluLocked()) {
        operandForwardStall = ofu->stall(inputBuffer.data(),
                                           inputBuffer.dataAddr());

        if (!operandForwardStall) begin();
    }
    // Increment the fetchoffset per potential fetch in next cycle, if begin is 
    // not called (as it will do the job otherwise)
    // Problematic

    if (!pushed) pipeEve.schedule();

    // schedule initCheck to run in next cycle
    executeEve.schedule(cpu->nextCycle());
}

void 
InstExecute::begin()
{
    switchTC();
    stageState(Busy);
    DPRINTF(ProtoExecuteInfo,"ALU: Executing ! \n");
    executeInst();
}

void 
InstExecute::executeInst()
{   
    assert(stageState() == Busy);
    stageState(Executing);

    SimpleThread * trd_ptr = threadCtxt()->thread;
    TheISA::PCState pc_state = trd_ptr->pcState();

    aluInst= inputBuffer.unload();
    DPRINTF(ProtoExecuteStage,"Cycle: %u ALU: %s at %#010x \n",
            uint64_t(cpu->curCycle()),aluInst->getName(),
            inputBuffer.dataAddr());

    if (ctrledPC != noAddr) assert(inputBuffer.dataAddr() == ctrledPC);

    if (aluInst && aluInst->isMemRef()) {
        DPRINTF(ProtoExecuteStage,"Cycle: %u ALU: Executing memory instruction"
                ".\n",uint64_t(cpu->curCycle()));

        Fault fault = aluInst->initiateAcc(threadCtxt(),cpu->traceData);
        ctrledPC = noAddr;
    } else if (aluInst && aluInst->isControl()) {
        
        TheISA::Decoder *dcoder = &(trd_ptr->decoder);
        bool compressed = dcoder->compressed(aluInst->machInst);

        Addr Npc = 0;
        if (compressed) Npc=sizeof(MachInst)/2;
        else Npc=sizeof(MachInst);
        pc_state.npc(pc_state.instAddr()+Npc);
        trd_ptr->pcState(pc_state);

        DPRINTF(ProtoExecuteInfo,"ALU: NPC %#10x !\n",
                trd_ptr->pcState().npc());
        DPRINTF(ProtoExecuteStage,"Cycle: %u ALU: Executing control "
                "instruction. Current PC : %#10x ! InstAddr : %#10x !\n",
                uint64_t(cpu->curCycle()), pc_state.pc(), pc_state.instAddr());

        //Execute
        Fault fault = aluInst->execute(threadCtxt(), cpu->traceData);

        DPRINTF(ProtoExecuteInfo,"ALU: After exec NPC:%#10x PC:%#10x !\n",
                trd_ptr->pcState().npc(),trd_ptr->pcState().instAddr());

        TheISA::PCState pc_state = trd_ptr->pcState();
        // TheISA::Decoder *dcoder = &(trd_ptr->decoder);
        // bool compressed = dcoder->compressed(alu_inst->machInst);
        pc_state.compressed(compressed);


        if (pc_state.branching()) 
            DPRINTF(ProtoExecuteStage,"ALU: Branching.\n");

        threadCtxt()->stayAtPC = false;
        threadCtxt()->fetchOffset = 0;
        TheISA::advancePC(pc_state, aluInst);
        trd_ptr->pcState(pc_state);


        dePipe.feedbk(Pipe<DEpipe_buffer>::_pcValid);
        // pc_status.validate();
        ctrledPC = pc_state.pc();
        DPRINTF(ProtoExecuteStage,"ALU: Jump to %#10x \n", ctrledPC);

    } else if (aluInst) {
        TheISA::PCState pc_state = trd_ptr->pcState();

        DPRINTF(ProtoExecuteStage,"Cycle: %u ALU: Executing non-memory "
        "instruction. Current PC : %#10x ! InstAddr : %#10x !\n",
        uint64_t(cpu->curCycle()), pc_state.pc(), pc_state.instAddr());

        //Execute
        Fault fault = aluInst->execute(threadCtxt(), cpu->traceData);

        ctrledPC = noAddr;
    } else {
        fatal("Execute: This should happen in decode stage, not in ALU \n");
    }    
    
    internalBuffer.load(aluInst,inputBuffer.dataAddr());
    instDependentCheck();
}

void 
InstExecute::instDependentCheck()
{
    assert(stageState() == Executing);

    if (aluInst->opClass() == OpClass::IntMult) {   
        execEndEve.schedule(cpu->clockEdge(Cycles(cpu->mulStages-1)));
        return;
    } else if(aluInst->opClass() == OpClass::IntDiv) {
        execEndEve.schedule(cpu->clockEdge(Cycles(cpu->divStages-1)));
        return;
    } else {
        executeComplete();
    }
}

void 
InstExecute::emPipePush()
{
    //Should occur only once per instruction.
    if (aluInst && !aluInst->isControl()) {

        SimpleThread * trd_ptr = threadCtxt()->thread;
        TheISA::PCState pc_state = trd_ptr->pcState();
        TheISA::Decoder *dcoder = &(trd_ptr->decoder);
        bool compressed = dcoder->compressed(aluInst->machInst);
        Addr Npc = 0;
        if (compressed) Npc=sizeof(MachInst)/2;
        else Npc=sizeof(MachInst);
        pc_state.npc(pc_state.instAddr()+Npc);

        TheISA::advancePC(pc_state, aluInst);
        trd_ptr->pcState(pc_state);    
        assert(threadCtxt()->prePC > 0);
        threadCtxt()->prePC -= Npc;
    }
    if (aluInst) DPRINTF(ProtoEMpipe,"Cycle: %u ALU: inst : %s, NPC: "
                          "%#10x, PC: %#10x in EM pipe\n",
                          uint64_t(cpu->curCycle()), aluInst->getName(),
                          threadCtxt()->thread->pcState().npc(),
                          threadCtxt()->thread->pcState().pc());
    aluInst=NULL;
    pushed = false;
    if (!isAluPushLocked()) pushed = emPipe.push(internalBuffer);
    
    if (pushed) {
        DPRINTF(ProtoEMpipe,"Cycle: %u ALU: Pushed data to EM pipe\n",
                uint64_t(cpu->curCycle()));
        if (internalBuffer.data()->isMemRef()) aluPushLock();
        // else if (internalBuffer.data()->isStore()) aluPushLock();
    }
    //just for testing purposes only
    // else {pushed = true; internalBuffer.unload();emPipe.reset();}

}
#endif