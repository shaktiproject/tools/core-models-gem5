#ifndef __CPU_PROTOCPU_INST_DECODE_CC__
#define __CPU_PROTOCPU_INST_DECODE_CC__

#include "cpu/protocpu/inst_decode.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

//Initializing the global kinda var
int InstDecode::decodeStagesCount = 0;

void 
InstDecode::NegEdgeEvent::schedule()
{
    // DPRINTF(ProtoDEpipe,"Scheduled DE push  \n");
    assert(!this->scheduled());
    idu->cpu->schedule(this, (idu->cpu->clockEdge())+1);
}

void 
InstDecode::PostEdgeEvent::schedule()
{
    assert(!this->scheduled());
    idu->cpu->schedule(this, idu->cpu->clockEdge()+2);
}

int 
InstDecode::decodeID()
{
    return _decodeID;
}

void 
InstDecode::start()
{
    switchTC();
    decodeEve.schedule();
}

void 
InstDecode::stop()
{
    if (decodeEve.scheduled()) cpu->deschedule(decodeEve);
    if (pipeEve.scheduled()) cpu->deschedule(pipeEve);
}

void 
InstDecode::feedbackCheck()
{
    if (dePipe.pcValid()) fdPipe.feedbk(Pipe<FDpipe_buffer>::_pcValid);
}

void 
InstDecode::initCheck()
{
    DPRINTF(ProtoDEpipe,"Cycle : %u Decode: %d filled in DE pipe\n",
            uint64_t(cpu->curCycle()), dePipe.quantity());
    feedbackCheck();
    assert(stageState() == Avail);

    if (inputBuffer.touched() == true && internalBuffer.touched() == true) 
        pulled = fdPipe.pop(&inputBuffer);

    if (inputBuffer.data() != noInst && internalBuffer.touched() == true)
        begin();

    // Increment the fetchoffset per potential fetch in next cycle, if begin is 
    // not called (as it will do the job otherwise)
    // Problematic

    if (!pushed) pipeEve.schedule();

    decodeEve.schedule();
}

void 
InstDecode::begin()
{
    switchTC();
    stageState(Busy);

    DPRINTF(ProtoDecodeInfo,"Decode: Decoding !\n");

    SimpleThread *trd_ptr = threadCtxt()->thread;
    //Assigning value zero to the zeroregister
    trd_ptr->setIntReg(ZeroReg,0);

    // resets predicates
    threadCtxt()->setPredicate(true);
    threadCtxt()->setMemAccPredicate(true);

    // check for instruction-count-based events
    trd_ptr->comInstEventQueue.serviceEvents(threadCtxt()->numInst);    
    decodeInst();
}

void 
InstDecode::decodeInst()
{   
    assert(stageState() == Busy);
    stageState(Decoding);

    SimpleThread * trd_ptr = threadCtxt()->thread;
    TheISA::PCState pc_state = trd_ptr->pcState();
    /*!
        @brief  Interesting. Duplicate the current pcState, do the necessary 
                tests & 
                modifications, if pcState expected is arrived, merge it with 
                original.
     */
    
    StaticInstPtr inst_ptr = NULL;
    TheISA::Decoder *dcoder = &(trd_ptr->decoder);
    instPC = pc_state.instAddr()+threadCtxt()->prePC;
    MachInst inst_hex = letoh(inputBuffer.data());
    
    Addr npc = 0;
    bool stay_at_PC = false;
    Addr fetch_offset = 0;
    
    if (inputBuffer.touched() == false)
        DPRINTF(ProtoDecodeStage,"Cycle: %u Decode: Received data from fetch :"
                " %#010x\n", uint64_t(cpu->curCycle()), inst_hex);

    //Decoder logic
    ExtMachInst decode_inst;

    //the bool also handles 2byte algined jumps
    bool inst_lower_hw_valid = ((pc_state.pc()+threadCtxt()->prePC) % 
                                sizeof(MachInst)) == 0;
    if (bufferedInst == noInst && inputBuffer.touched() == false) {
        
        if (inst_lower_hw_valid) {
        
            if (dcoder->compressed(inst_hex)) {
                decode_inst = inst_hex & lowerBitMask;
                inst_ptr = dcoder->decode(decode_inst, pc_state.instAddr());
                bufferedInst = (inst_hex & upperBitMask) >> 16;
                //Pc update
                npc = sizeof(MachInst)/2;
                fetch_offset = sizeof(MachInst)/2;
            } else {
                decode_inst = inst_hex;
                inst_ptr = dcoder->decode(decode_inst, pc_state.instAddr());
                bufferedInst = noInst;

                npc = sizeof(MachInst);
                fetch_offset = 0;
            }
        } else {
                    
            decode_inst = (inst_hex & upperBitMask) >> 16;

            if (dcoder->compressed(decode_inst)) {
                inst_ptr = dcoder->decode(decode_inst, pc_state.instAddr());
                bufferedInst = noInst;

                npc = sizeof(MachInst)/2;
            } else {
                inst_ptr = nullptr;
                bufferedInst = decode_inst;

                stay_at_PC = true;
                fetch_offset = sizeof(MachInst)/2;
            }
        }
        inputBuffer.unload();
    } else if (bufferedInst != noInst) {

        assert((bufferedInst & upperBitMask) == 0);

        // only 16 bits of vaild data are stored in lower two bytes.
        decode_inst = bufferedInst;
        if (dcoder->compressed(decode_inst)) {
            inst_ptr = dcoder->decode(decode_inst, pc_state.instAddr());
            bufferedInst = noInst;

            npc = sizeof(MachInst)/2;
            
            if (fdPipe.empty()) {//Keep an eye on this
                //Needed for buffered stages or redundant ?
                fetch_offset = sizeof(MachInst); 
            }
            
        } else if (inputBuffer.touched() == false) {
            //entire 32 bits contains a value
            decode_inst |= (inst_hex & lowerBitMask) << 16;
            inst_ptr = dcoder->decode(decode_inst, pc_state.instAddr());
            bufferedInst = (inst_hex & upperBitMask) >> 16;
            inputBuffer.unload();

            npc = sizeof(MachInst);
            fetch_offset = sizeof(MachInst)/2;
        }
    }

    instDependentUpdate(inst_ptr, npc, stay_at_PC, fetch_offset);
}

void 
InstDecode::instDependentUpdate(StaticInstPtr &inst_ptr, Addr npc, bool stay_at_PC
                                , Addr fetch_offset)
{
    
    assert(stageState() == Decoding);

    SimpleThread * trd_ptr = threadCtxt()->thread;
    TheISA::PCState pc_state = trd_ptr->pcState();
    threadCtxt()->stayAtPC = stay_at_PC;
    threadCtxt()->fetchOffset = fetch_offset;

    if (inst_ptr) {
        if (inst_ptr->isControl()) {
            fdPipe.feedbk(Pipe<FDpipe_buffer>::_pcInvalid);
            refresh();
            // pc_status.invalidate();
            // icache_pkt_status.invalidate();//should be moved to alu and 
            // executed if the current fetch PC is not as calculated by ALU.
            //pc not ready, this should block the fetch in the same cycle. will
            // implement
        } else {
            threadCtxt()->prePC += npc;
            /// pcState.npc(pcState.instAddr()+npc);
            /// TheISA::advancePC(pcState, inst_ptr);
            /// trd_ptr->pcState(pcState);
        }
    } else {
        // npc here should be == 0 always I guess
        assert(npc == 0 );
        // pcState.npc(pcState.instAddr()+npc);
        // trd_ptr->pcState(pcState);
    }

    if (inputBuffer.touched() == true) {
        DPRINTF(ProtoDecodeInfo,"Cycle: %u Decode: Incrementing PC 0x%#08x and"
                "the instAddr %#010x with fetchOffset %d and prePC %d\n",
                uint64_t(cpu->curCycle()), pc_state.pc(),pc_state.instAddr(), 
                threadCtxt()->fetchOffset, threadCtxt()->prePC);
    }
    
    if(inst_ptr) {   
        currentInst = inst_ptr;
        if ((inst_ptr->machInst & 0x3) < 0x3) {
        DPRINTF(ProtoDecodeStage,"Cycle: %u Decode: Decoded instruction: "
                "(%s)%#06x at %#010x\n", uint64_t(cpu->curCycle()),
                currentInst->getName(), currentInst->machInst, instPC);
        } else {
        DPRINTF(ProtoDecodeStage,"Cycle: %u Decode: Decoded instruction: "
                "(%s)%#010x at %#010x\n", uint64_t(cpu->curCycle()),
                currentInst->getName(), currentInst->machInst, instPC);
        }
    } else {
        currentInst = nullptr;
        // panic("This shouldn't occur after the first time \n");
    }

    // #if TRACING_ON    
    // if(current_inst)
    // {
    //     cpu->traceData = cpu->tracer->getInstRecord(curTick(), 
    //                      trd_ptr->getTC(),current_inst, trd_ptr->pcState());
    // }
    // #endif

    if (currentInst) {
        internalBuffer.load(currentInst,instPC);
        pipeEve.schedule();
    }
    stageState(Avail);
}

void 
InstDecode::dePipePush()
{
    pushed = dePipe.push(internalBuffer);
    if (pushed) DPRINTF(ProtoDEpipe,"Cycle: %u Decode: Pushed data to DE pipe"
                        "\n", uint64_t(cpu->curCycle()));
}
#endif