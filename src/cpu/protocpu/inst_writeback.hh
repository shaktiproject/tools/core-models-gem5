#ifndef __CPU_PROTOCPU_WRITEBACK_HH__
#define __CPU_PROTOCPU_WRITEBACK_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoWritebackInfo.hh"
#include "debug/ProtoWritebackStage.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

class InstWriteback{

public:
    static int writebackStagesCount;

private:
    const Addr noAddr = -1;

    int _writebackID;
    ProtoContext *_threadCtxt;
    ProtoContext *_switchTC;
    ProtoCPU *cpu;
    StaticInstPtr wbInst;
    Pipe<MWpipe_buffer>& mwPipe;
    Pipe<OFpipe_buffer>& ofPipe;

    bool pulled;
    bool pushed;

    enum writebackStatus 
    {
        Busy,
        Accessing_registers,
        Avail,
    };
    writebackStatus _stageState;

    struct PostEdgeEvent: public Event
    {
        InstWriteback *iwu;
        PostEdgeEvent(InstWriteback *_iwu) : iwu(_iwu) {}
        
        const char
        *description() const
        { 
            return "Writeback stage %d's personal post -ve edge scheduler for "
                   "combinatorial circuits"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void 
        process()
        {    
            iwu->initCheck();
        }
    };
    PostEdgeEvent writebackEve;

    struct NegEdgeEvent: public Event
    {
        InstWriteback *iwu;
        NegEdgeEvent(InstWriteback *_iwu) : iwu(_iwu) {}
        
        const char 
        *description() const 
        { 
            return "Writeback stage %d's personal -ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void
        process()
        {
            iwu->negEdgeEvents();
        }
    };
    NegEdgeEvent pipeEve;
    
    MWpipe_buffer inputBuffer;
public:
    OFpipe_buffer internalBuffer;


public:
    InstWriteback(Pipe<MWpipe_buffer>& mw_pipe,ProtoCPU * _cpu,
                  Pipe<OFpipe_buffer>& of_pipe):
    _threadCtxt(NULL),
    _switchTC(NULL),
    cpu(_cpu),
    wbInst(NULL),
    mwPipe(mw_pipe),
    ofPipe(of_pipe),
    pulled(true),
    pushed(true),
    _stageState(Avail),
    writebackEve(this),
    pipeEve(this),
    inputBuffer(NULL),
    internalBuffer(NULL)
    {
       writebackStagesCount++;
        _writebackID=writebackStagesCount;
        DPRINTF(ProtoWritebackStage,"Writeback stage %d activated\n",
                writebackStagesCount);
    }


    void start();
    void stop();
    void initCheck();
    void writebackInst();
    int writebackID();

    void 
    switchTC(ProtoContext *p)
    {
        _switchTC = p;
    }

private:
    void feedbackCheck();
    void begin();
    void ofPipePush();
    void negEdgeEvents(){}

    void
    writebackComplete()
    {
        assert(stageState()==Accessing_registers);
        stageState(Avail);
        // wb_inst=NULL;
        pipeEve.schedule();
    }

    void 
    switchTC()
    {
        assert(stageState() == Avail);
        threadCtxt(_switchTC);
    }
    
    void
    stageState(writebackStatus f_s)
    {
        _stageState = f_s;
    }
    
    writebackStatus
    stageState()
    {
        return _stageState;
    }
    
    void
    threadCtxtRst(void)
    {
        _threadCtxt = NULL;
    }
    
    void
    threadCtxt(ProtoContext *t)
    {
        _threadCtxt = t;
    }
    
    ProtoContext
    * threadCtxt()
    {
        return _threadCtxt;
    }
};



#endif