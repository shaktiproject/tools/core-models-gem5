#ifndef __CPU_PROTOCPU_WRITEBACK_CC__
#define __CPU_PROTOCPU_WRITEBACK_CC__

#include "cpu/protocpu/inst_writeback.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

//Initializing the global kinda var
int InstWriteback::writebackStagesCount = 0;

void
InstWriteback::NegEdgeEvent::schedule()
{
    assert(!this->scheduled());
    iwu->cpu->schedule(this, (iwu->cpu->clockEdge())+1);
}

void
InstWriteback::PostEdgeEvent::schedule()
{
    assert(!this->scheduled());
    iwu->cpu->schedule(this, (iwu->cpu->clockEdge())+2);
}

int
InstWriteback::writebackID()
{
    return _writebackID;
}

void
InstWriteback::start()
{
    switchTC();
    writebackEve.schedule();
}

void
InstWriteback::stop()
{
    if (writebackEve.scheduled()) cpu->deschedule(writebackEve);
    if (pipeEve.scheduled()) cpu->deschedule(pipeEve);
}

void InstWriteback::feedbackCheck(){}

void InstWriteback::initCheck(){

    feedbackCheck();

    if(inputBuffer.touched() == true) pulled = mwPipe.pop(&inputBuffer);

    if(stageState() == Avail && !inputBuffer.touched() && 
       internalBuffer.touched())
        begin();

    if (!pushed) ofPipePush();

    writebackEve.schedule();
}

void
InstWriteback::begin()
{
    switchTC();
    stageState(Busy);

    wbInst = inputBuffer.unload();
    writebackInst();
}

void
InstWriteback::writebackInst()
{
    assert(stageState() == Busy);
    stageState(Accessing_registers);
    internalBuffer.load(wbInst,inputBuffer.dataAddr());
    DPRINTF(ProtoWritebackStage,"Cycle: %u Writeback: Retiring instruction %s "
            "at %#010x\n", uint64_t(cpu->curCycle()), wbInst->getName(),
            inputBuffer.dataAddr());
    ofPipePush();
}

void 
InstWriteback::ofPipePush()
{
    //Killing the instruction    
    assert(stageState() == Accessing_registers);
    assert(ofPipe.push(internalBuffer));
    wbInst=NULL;
    writebackComplete();
    DPRINTF(ProtoWritebackInfo,"Cycle: %u Writeback: The OF pipe quantity %d\n"
            , uint64_t(cpu->curCycle()), ofPipe.quantity());
    cpu->opFwdPushEvent();
}
#endif