#ifndef __CPU_PROTOCPU_STAGE_BUFFER_HH__
#define __CPU_PROTOCPU_STAGE_BUFFER_HH__

#include "cpu/base.hh"

template <typename vital_data>
class StageBuffer
{
  private:
   Addr noAddr = -1;

  protected:
    vital_data data_rst;
    vital_data _data;
    Addr _dataAddr;
    bool _touched;

    bool 
    dataAddr(Addr a)
    {
        if (a != noAddr) {
            _dataAddr = a;
            return true;
        } else return false; 
    }

    bool 
    data(vital_data vd)
    {
        if (vd != data_rst) {
            _data = vd;
            return true;
        } else return false; 
    }
    
    void 
    touched(bool b)
    {
        _touched = b;
    }

public:

    StageBuffer(vital_data data_reset_value): 
    data_rst(data_reset_value),
    _data(data_rst),
    _dataAddr(noAddr),
    _touched(true)
    {}

    Addr  
    dataAddr()
    {
        return _dataAddr;
    }

    vital_data
    data()
    {
        return _data;
    }

    bool
    touched()
    {
        return _touched;
    }

    void
    load(vital_data VD, Addr A)
    {
        assert(touched() == true);
        assert(data(VD));
        assert(dataAddr(A));
        touched(false);
    }

    vital_data
    unload()
    {
        assert(touched() == false);
        touched(true);
        return data();
    }

    void
    reset()
    {
        _data = data_rst;
        _dataAddr = noAddr;
        _touched = true;
    }
};

typedef StageBuffer<TheISA::MachInst> FDpipe_buffer;
typedef StageBuffer<StaticInstPtr> DEpipe_buffer;
typedef StageBuffer<StaticInstPtr> EMpipe_buffer;
typedef StageBuffer<StaticInstPtr> MWpipe_buffer;
typedef StageBuffer<StaticInstPtr> OFpipe_buffer;

#endif