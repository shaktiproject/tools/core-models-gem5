#ifndef __CPU_PROTOCPU_OPERAND_FORWARD_CC__
#define __CPU_PROTOCPU_OPERAND_FORWARD_CC__

#include "cpu/protocpu/operand_forward.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

//Initializing the global kinda var
int OperandForward::operandForwardStagesCount = 0;


int
OperandForward::operandForwardID()
{
    return _operandForwardID;
}

bool
OperandForward::stall(StaticInstPtr incoming_inst, Addr inst_addr)
{
    assert(stageState()==Avail);
    stageState(Checking_dependency);

    if (!destInst.data())
        DPRINTF(ProtoOperandForwardStage,"Cycle: %u OperandForward: Checking "
                "instruction %s at %#010x\n",uint64_t(cpu->curCycle())
                , incoming_inst->getName(), inst_addr);

    sourceInst.load(incoming_inst,inst_addr);
    if (destInst.data()) {
        
        DPRINTF(ProtoOperandForwardStage,"Cycle: %u OperandForward: Checking "
                "instruction %s at %#010x vs %s at %#010x\n",
                uint64_t(cpu->curCycle()), incoming_inst->getName(), inst_addr,
                destInst.data()->getName(), destInst.dataAddr());
        
        // ALU should not send same instruction over and over again : 
        // But it can if there is a jump to same address
        
        // can't retire more inst than what alu is providing. 
        // This should never fail.
        assert(ofPipe.quantity() == 0); 

        if (destInst.data()->numDestRegs() != 0 ) {
            int numSrcRegs = sourceInst.data()->numSrcRegs();    
            RegIndex destReg = destInst.data()->destRegIdx(0).index();

            for (int i = 0;i<numSrcRegs;i++) {
                RegIndex srcReg = sourceInst.data()->srcRegIdx(i).index();
                if (srcReg == destReg) {
                    if (!destRegRetired()) {
                        sourceInst.reset();
                        stageState(Avail);
                        return true;    
                    }
                }
            }
        }
    }
    operandForwardComplete();
    return false;
}


// void OperandForward::feedbackCheck(){

// }

//Utilized by proto.cc and called only in writeback
// ofPipe connects writeback stage to this
void
OperandForward::ofPipeCheck()
{
    assert(ofPipe.pop(&inputBuffer));
    destRegRetired();
}

// Check if the retired instruction from writeback stage matches the destInst. 
// If yes,reset the destInst as that instruction has updated its dest register
bool
OperandForward::destRegRetired()
{
    DPRINTF(ProtoOperandForwardInfo,"Cycle: %u OperandForward: Prev inst addr"
            " %#010x vs retiring inst addr %#010x\n", uint64_t(cpu->curCycle())
            , destInst.dataAddr(), inputBuffer.dataAddr());
   
    // The first check can be an assert but can cause legitimate failure in the
    // begining when the first is mem acc and second is dependent then the 
    // input buffer is required to be not null even before first pop - which is
    // bogus.
    // The input buffer is updated only in the writeback stage and it should be
    // equal to our destination data. If yes the instruction has retired hence

    // reset destInst
    if (inputBuffer.data() && destInst.data() == inputBuffer.data() &&
        destInst.dataAddr() == inputBuffer.dataAddr()) {
        DPRINTF(ProtoOperandForwardStage,"Cycle: %u OperandForward: "
                "Destination register updated for instruction %s at %#010x\n",
                uint64_t(cpu->curCycle()), destInst.data()->getName(),
                destInst.dataAddr());

        inputBuffer.reset();
        destInst.reset();
        return true;
    }
    inputBuffer.reset();
    // Don't reset the buffer as we may need it or nope
    // (ALU stalls, the executed inst gonna retire or get stuck at memacc and
    // the future executed inst may need this to confirm. But future is always
    // +1 cycle apart so We can reset it.)
    return false;
}

//Update the destination data with the in-came data
//Only do this if the 
void
OperandForward::operandForwardComplete()
{
        assert(stageState()==Checking_dependency);
        stageState(Avail);

        // Is overrided by next statement but touched should be true and it 
        // should have happened in destRegRetired. 
        // But if there is no dependency we can update the destInst data. 
        // Hence needs a reset
        destInst.reset();
        destInst.load(sourceInst.data(),sourceInst.dataAddr());
        sourceInst.reset();
        // input_buffer.reset();
}
#endif