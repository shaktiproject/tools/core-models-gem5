#ifndef __CPU_PROTOCPU_INST_FETCH_CC__
#define __CPU_PROTOCPU_INST_FETCH_CC__

#include "cpu/protocpu/inst_fetch.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

    // Input - PC
    // status - avail ?
    // Output - internal_buffer_available ?

//Initializing the global kinda var
int InstFetch::fetchStagesCount = 0;

void 
InstFetch::NegEdgeEvent::schedule()
{
    assert(!this->scheduled());
    ifu->cpu->schedule(this, curTick()+1);
}

void 
InstFetch::PosEdgeEvent::schedule(Tick t)
{
    assert(!this->scheduled());
    ifu->cpu->schedule(this, t);
}

void 
InstFetch::start()
{
    switchTC();
    fetchEve.schedule(cpu->clockEdge());
}

void 
InstFetch::stop()
{
    if (fetchEve.scheduled()) cpu->deschedule(fetchEve);
    if(pipeEve.scheduled()) cpu->deschedule(pipeEve);
}
void 
InstFetch::feedbackCheck()
{
    if (fdPipe.pcInvalid()) {
        fdPipe.reset();
        fetchLock();//prevent ongoing fetch from polluting the pipe in future;
    }

    if (stageState() == Avail && fdPipe.pcValid()) fetchUnlock();
}

void 
InstFetch::initCheck()
{   
    DPRINTF(ProtoFDpipe,"Cycle: %u Fetch: %d filled \n",
            uint64_t(cpu->curCycle()), fdPipe.quantity());
    feedbackCheck();
    if (stageState() == Avail && internalBuffer.touched() == true && 
        !isFetchLocked()) { // or pipe buffer is full or PC invalid
        if (!fdPipe.empty()) 
            threadCtxt()->fetchOffset = fdPipe.quantity()* 
                                        sizeof(TheISA::MachInst);

        translateAddr(begin());
    }

    if (!pushed) pipeEve.schedule();
    fetchEve.schedule(cpu->nextCycle());
}
        
RequestPtr 
InstFetch::begin()
{
    switchTC();
    stageState(Busy);
    assert(!isFetchLocked());

    //Prepare necessary data
    SimpleThread *trd_ptr = threadCtxt()->thread;

    //Begin preparing request info
    RequestPtr ifetch_request = std::make_shared<Request>();
    ifetch_request->taskId(cpu->taskId());
    ifetch_request->setContext(trd_ptr->contextId());

    //calculate PC
    fetchPC = calculateAddr(threadCtxt());    

    DPRINTF(ProtoFetchStage, "Cycle: %u Fetch: Fetching instruction from "
            "%#010x \n", uint64_t(cpu->curCycle()), fetchPC);
    ifetch_request->setVirt(fetchPC, sizeof(MachInst), Request::INST_FETCH,
                            cpu->instMasterId(), trd_ptr->instAddr());

    return ifetch_request;
}

void 
InstFetch::translateAddr(RequestPtr ifetch_request )
{
    if (FullSystem) {
        ifetch_request->setPaddr(ifetch_request->getVaddr());
        sendReq(ifetch_request, threadCtxt()->thread->getTC());
    } else {
        threadCtxt()->thread->itb->translateTiming(ifetch_request,
                                                  threadCtxt()->thread->getTC()
                                                  , &(cpu->instTranslate), 
                                                  BaseTLB::Execute);
    }
}

void 
InstFetch::sendReq(const RequestPtr &req, ThreadContext *tc)
{
    assert(stageState() == Busy);
    stageState(Try_send);

    icachePkt = new Packet(req, MemCmd::ReadReq);
    icachePkt->dataStatic(&inst);
    if (iport.sendTimingReq(icachePkt)) {
        stageState(Waiting_for_valid_resp);
        icachePkt = NULL;
    } else {
        icachePktBlocked = icachePkt;
        icachePkt = NULL;
        DPRINTF(ProtoFetchStage, "Cycle: %u Fetch: Icache is busy. Try "
                "resending after it's free !\n", uint64_t(cpu->curCycle()));
    }
}

void 
InstFetch::resendReq()
{
    assert(stageState() == Try_send || stageState() == Try_resend);
    stageState(Try_resend);

    assert(icachePktBlocked != NULL);
    if (iport.sendTimingReq(icachePktBlocked)) {
        icachePktBlocked = NULL;
        stageState(Waiting_for_valid_resp);
    } else 
        DPRINTF(ProtoFetchStage, "Cycle: %u Fetch: Icache is busy. Try "
                "resending after it's free ! \n", uint64_t(cpu->curCycle()));
}

FDpipe_buffer 
&InstFetch::recvInst(PacketPtr pkt)
{
    assert(stageState() == Waiting_for_valid_resp);
    stageState(Avail);
    icachePktReceived = pkt;

    if (isFetchLocked()) {
        DPRINTF(ProtoFetchStage, "Fetch: fetch ditched! \n");
        internalBuffer.reset();
    } else {
        internalBuffer.load(inst,fetchPC);
        pipeEve.schedule();
        DPRINTF(ProtoFetchInfo, "Fetch: fetched! \n");
    }
    inst = noInst;
    fetchPC = noAddr; 
    return internalBuffer;
}

void 
InstFetch::fdPipePush()
{
    pushed = fdPipe.push(internalBuffer);
    if (pushed) DPRINTF(ProtoFDpipe,"Cycle: %u Fetch: Pushed data to FD pipe\n"
                        , uint64_t(cpu->curCycle()));
}  

int 
InstFetch::fetchID()
{
    return _fetchID;
}

Addr 
InstFetch::calculateAddr(ProtoContext *trd_ctxt)
{ 
    Addr inst_addr = trd_ctxt->thread->instAddr();
    Addr fetch_PC = (inst_addr) + trd_ctxt->fetchOffset + trd_ctxt->prePC;
    // if I cache is not 4 byte aligned. Gem5 doesn't care at all.
    if (fetch_PC % sizeof(MachInst) == sizeof(MachInst)/2)
        fetch_PC -= sizeof(MachInst)/2;

    else if ((fetch_PC % sizeof(MachInst)) != 0) assert(0);

    return fetch_PC;
}

#endif