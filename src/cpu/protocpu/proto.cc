#ifndef __CPU_PROTOCPU_PROTO_CC__
#define __CPU_PROTOCPU_PROTO_CC__

#include "cpu/protocpu/proto_cc.h"

ProtoCPU::ProtoCPU(ProtoCPUParams *prmtr) : 
BaseCPU(prmtr),
mulStages(prmtr->mulStages),
divStages(prmtr->divStages),
dcachePkt(NULL),
branchPred(prmtr->branchPred),
traceData(nullptr),
ofPipe(1),
opfwdOne(new OperandForward(ofPipe,this)),
iport(this),
instTranslate(this), 
fdPipe(10),
fetchOne(new InstFetch(this,iport,fdPipe)),
dePipe(1),
decodeOne(new InstDecode(fdPipe,this,dePipe)),
emPipe(1),
executeOne(new InstExecute(dePipe,this,opfwdOne,emPipe)),
mwPipe(1),
memaccOne(new InstMemacc(emPipe,this,mwPipe)),
writebackOne(new InstWriteback(mwPipe,this,ofPipe)),
dport(this)
{
    for (int trd_num = 0;trd_num<numThreads;trd_num++) {
        if (FullSystem) {
            thread = new SimpleThread(this, trd_num, prmtr->system, 
            prmtr->itb, prmtr->dtb, prmtr->isa[trd_num]); 
            DPRINTF(ProtoCPU,"Full system simulation using ProtoCPU \n");
        } else {
            thread = new SimpleThread(this, trd_num, prmtr->system, 
                                      prmtr->workload[trd_num],
                                      prmtr->itb, prmtr->dtb,
                                      prmtr->isa[trd_num]);        
            DPRINTF(ProtoCPU,"System emulated simulation using ProtoCPU \n");
        }
        threadQueue.push_back(thread);
        ThreadContext *tc = thread->getTC();
        threadContexts.push_back(tc);
    }
    assert(!threadQueue.empty());
    DPRINTF(ProtoCPU,"Number of divider stages: %d and number of multiplier "
            "stages: %d \n",divStages, mulStages);
}
/*!
    @fn ProtoCPU::ProtoCPU(ProtoCPUParams *prmtr)
    @brief  Constructor of class ProtoCPU. 
            Initializes the variables and pointers used by this class
            Calls the constructors of objects nested.
        @param  this 
                A pointer to a particular instance of this class
        @param  prmtr
                A pointer to the parameters passed to the ProtoCPU object 
            upon begining of simulation (from python config script I guess)

    @param  BaseCPU(prmtr)
            Constructor of ProtoCPU's parent class.
    @param  icache_pkt(NULL)
            Initializing this PacketPtr with null. 
    @param  icache_pkt_blocked(NULL)
            Initializing this PacketPtr with null. 
    @param  icache_recv_pkt(NULL)
            Initializing this PacketPtr with null. 
    @param  dcachePkt(NULL)
            Initializing this PacketPtr with null. 
    @param  iFetchEve([this]{ iFetch_init(); }, name())
            Constructor. Inherited from EventFunctionWrapper.
            Useful object to schedule the function ProtoCPU::iFetch_init()
    @param  dcodeEve([this]{ protocpu_dcode(); }, name())
            Constructor. Inherited from EventFunctionWrapper.
            Useful to schedule the function ProtoCPU::protocpu_dcode()
    @param  aluEve([this]{ protocpu_alu(); }, name())
            Constructor. Inherited from EventFunctionWrapper.
            Useful to schedule the function ProtoCPU::protocpu_alu()            
    @param  memAccEve([this]{ protocpu_memAcc(); }, name())
            Constructor. Inherited from EventFunctionWrapper.
            Useful to schedule the function ProtoCPU::protocpu_memAcc() 
    @param  regWriteEve([this]{ protocpu_regWrite(); }, name()) 
            Constructor. Inherited from EventFunctionWrapper.
            Useful to schedule the function ProtoCPU::protocpu_regWrite() 
    @param  traceData
            Yet to understand it's purpose
    @param  instTranslate(this)
            Constructor. Inherited from BaseTLB::Translation
            Responsible to activities related to instruction address
            translation
    @param  dport(this)
            Contains a complete set of registers with other variables
            necessary to keep track of the state of CPU.

    @param  thread
            Initializing the Hardware thread. 
            Contains a complete set of registers with other variables
            necessary to keep track of the state of CPU.
    @note   Currently, only one thread works.
            No support for Multicore system (?) , 
            Full system simulation, checker.  
    @fn     threadContexts.push_back(thread->getTC())
    @brief  Register's the current thread with thread context.
            Inherited from 'src/cpu/base' i.e. BaseCPU, 
            parent of ProtoCPU. (?) 

 */


void
ProtoCPU::init()
{
    //Assigning a Process to our HW thread in/if SE mode.
    BaseCPU::init();
    ThreadContext *tc = thread->getTC();
    tc->initMemProxies(tc);
}
/*!
    @fn ProtoCPU::init()
    @brief Called before simulation begins.
    @note   only override if BaseCPU::init() and additional
            functions are added. 
            No need to override if it just contains BaseCPU::init()
 */


void
ProtoCPU::verifyMemoryMode() const
{
    if (!system->isTimingMode())
        fatal("ProtoCPU is time conscious and req"
              "uires memory to be in TIMING mode.");
}
/*!
    @fn ProtoCPU::verifyMemoryMode()
    @brief  Check if the user connected non-timing memory to time 
            conscious ProtoCPU   
 */
void
ProtoCPU::checkPcEventQueue()
{
    Addr oldpc, pc = threadCtxt->thread->instAddr();
    do {
        oldpc = pc;
        threadCtxt->thread->pcEventQueue.service(
                oldpc, threadContexts[0]);
        pc = threadCtxt->thread->instAddr();
    } while (oldpc != pc);
}

void
ProtoCPU::activateContext(ThreadID thread_num)
{
    assert(thread_num<numThreads);
    if (ctxtQueue.empty()) {
        for (int trd_num = 0;trd_num<numThreads;trd_num++) {
            ctxtQueue.push_back(new ProtoContext(this ,threadQueue[trd_num]));
        }
    }
    cpuStatus = Running;
//     thread->activate();
    DPRINTF(ProtoCPU,"ProtoCPU: Activating context. \n");
    BaseCPU::activateContext(thread_num);    
   
    if (std::find(activeThreads.begin(), activeThreads.end(),
        thread_num)==activeThreads.end()) {
        activeThreads.push_front(thread_num);
    }
    curThread = activeThreads.front();
    thread = threadQueue[curThread];
    threadCtxt = ctxtQueue[curThread];

    fetchOne->switchTC(threadCtxt);
    fetchOne->start();
    decodeOne->switchTC(threadCtxt);
    decodeOne->start();
    executeOne->switchTC(threadCtxt);
    executeOne->start();
    memaccOne->switchTC(threadCtxt);
    memaccOne->start();
    writebackOne->switchTC(threadCtxt);
    writebackOne->start();
}
/*!
    @fn ProtoCPU::activateContext(ThreadID thread_num)
    @brief  Kinda the first ProtoCPU function called after the begining 
            of simulation. Kickstarts the CPU functions. Probably used 
            by gem5 only.
    @param[in] thread_num
            Probably passed by the caller         
    @note   The ProtoContext needs to be differently handled for SMT and
            MP systems 
 */

//Fetch stage
void
ProtoCPU::iFetch_send_ireq(const RequestPtr &req, ThreadContext *tc)
{
    fetchOne->sendReq(req,tc); 
}

void
ProtoCPU::iFetch_resend_ireq()
{
        // iFetch - n
    fetchOne->resendReq(); 
}

void
ProtoCPU::iFetch_recv_inst(PacketPtr pkt)
{
    fetchOne->recvInst(pkt); 
}

//Mem acc
void
ProtoCPU::completeDcacheAccess(PacketPtr pkt)
{
    memaccOne->memaccInst(pkt);
}

//Operand forward 
void
ProtoCPU::opFwdPushEvent()
{
    opfwdOne->ofPipeCheck();
}

void
ProtoCPU::threadSnoop(PacketPtr pkt, ThreadID sender)
{
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        if (tid != sender) {
            if (getCpuAddrMonitor(tid)->doMonitor(pkt)) wakeup(tid);

            TheISA::handleLockedSnoop(thread, pkt, dport.cacheBlockMask);
        }
    }
}

void
ProtoCPU::startup()
{
    BaseCPU::startup();
    assert(!threadQueue.empty());
    for (int trd_num = 0;trd_num<numThreads;trd_num++) {
        SimpleThread *thread = threadQueue[trd_num];
        thread->startup();
    }
}

ProtoCPU::~ProtoCPU()
{}
/*!
    @brief Destructor with no custom functions
 */

ProtoCPU
*ProtoCPUParams::create()
{
    return new ProtoCPU(this);
}
/*!
    @brief Called to instantiate an instance of ProtoCPU (?)
 */

#endif