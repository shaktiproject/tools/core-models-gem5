#ifndef __CPU_PROTOCPU_INST_DECODE_HH__
#define __CPU_PROTOCPU_INST_DECODE_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoDEpipe.hh"
#include "debug/ProtoDecodeStage.hh"
#include "debug/ProtoDecodeInfo.hh"
#include "debug/ProtoCPU.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

class InstDecode{

public:
    static int decodeStagesCount;

private:
    const MachInst lowerBitMask = (1 << sizeof(MachInst) * 4) - 1;
    const MachInst upperBitMask = lowerBitMask << sizeof(MachInst) * 4;

    const MachInst noInst = -1;
    const Addr noAddr = -1;

    int _decodeID;
    ProtoContext *_threadCtxt;
    ProtoContext *_switchTC;
    ProtoCPU *cpu;
    MachInst bufferedInst;
    Addr instPC;
    StaticInstPtr currentInst;
    Pipe<FDpipe_buffer>& fdPipe;
    Pipe<DEpipe_buffer>& dePipe;

    bool pulled;
    bool pushed;

    enum decodeStatus 
    {
        Busy,
        Decoding,
        Avail,
    };
    decodeStatus _stageState;

    struct PostEdgeEvent: public Event
    {
        InstDecode *idu;

        PostEdgeEvent(InstDecode *_idu) : idu(_idu) {}
        const char 
        *description() const 
        { 
            return "Decode stage %d's personal post -ve edge scheduler for "
                   "combinatorial circuits"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void 
        process()
        {    
            idu->initCheck();
        }
    };
    PostEdgeEvent decodeEve;

    struct NegEdgeEvent: public Event
    {
        InstDecode *idu;

        NegEdgeEvent(InstDecode *_idu) : idu(_idu) {}
        const char 
        *description() const 
        { 
            return "Decode stage %d's personal -ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void process(){
            idu->dePipePush();
        }
    };
    NegEdgeEvent pipeEve;
    
    FDpipe_buffer inputBuffer;

public:
    DEpipe_buffer internalBuffer;

public:
    InstDecode(Pipe<FDpipe_buffer>& fd_pipe,ProtoCPU * _cpu, 
               Pipe<DEpipe_buffer>& de_pipe):
    _threadCtxt(NULL),
    _switchTC(NULL),
    cpu(_cpu),
    bufferedInst(noInst),
    instPC(noAddr),
    currentInst(NULL),
    fdPipe(fd_pipe),
    dePipe(de_pipe),
    pulled(true),
    pushed(true),
    _stageState(Avail),
    decodeEve(this),
    pipeEve(this),
    inputBuffer(noInst),
    internalBuffer(NULL)
    {
       decodeStagesCount++;
        _decodeID=decodeStagesCount;
        DPRINTF(ProtoDecodeInfo,"Decode stage %d activated\n",
                decodeStagesCount);
    }

    Addr calculateAddr(ProtoContext *);
    void start();
    void stop();
    void initCheck();
    void feedbackCheck();
    void begin();
    void decodeInst();
    void instDependentUpdate(StaticInstPtr &inst_ptr, Addr npc,
                             bool stay_at_PC, Addr fetch_offset);
    void dePipePush();
    int decodeID();

    void 
    switchTC(ProtoContext *p)
    {
        _switchTC = p;
    }

private:
    void 
    refresh()
    {
        bufferedInst = noInst;
        inputBuffer.reset();
    }

    void 
    switchTC()
    {
        assert(stageState() == Avail);
        threadCtxt(_switchTC);
    }
    
    void 
    stageState(decodeStatus f_s)
    {
        _stageState = f_s;
    }
    
    decodeStatus 
    stageState()
    {
        return _stageState;
    }
    
    void 
    threadCtxtRst(void)
    {
        _threadCtxt = NULL;
    }
    
    void 
    threadCtxt(ProtoContext *t)
    {
        _threadCtxt = t;
    }
    
    ProtoContext 
    * threadCtxt()
    {
        return _threadCtxt;
    }
};



#endif