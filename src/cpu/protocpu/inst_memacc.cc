#ifndef __CPU_PROTOCPU_INST_MEMACC_CC__
#define __CPU_PROTOCPU_INST_MEMACC_CC__
#include "cpu/protocpu/inst_memacc.hh"
#include "cpu/protocpu/exec_context.hh"
#include "cpu/protocpu/proto.hh"

// Initializing the global kinda var
int InstMemacc::memaccStagesCount = 0;

void 
InstMemacc::NegEdgeEvent::schedule()
{
    assert(!this->scheduled());
    imu->cpu->schedule(this, curTick()+1);
}

void 
InstMemacc::PosEdgeEvent::schedule(Tick t)
{
    assert(!this->scheduled());
    imu->cpu->schedule(this, t);
}

int 
InstMemacc::memaccID()
{
    return _memaccID;
}

void 
InstMemacc::start()
{
    switchTC();
    memaccEve.schedule(cpu->clockEdge());
}

void 
InstMemacc::stop()
{
    if (memaccEve.scheduled()) cpu->deschedule(memaccEve);
    if (pipeEve.scheduled()) cpu->deschedule(pipeEve);
}

void InstMemacc::feedbackCheck() {}

void 
InstMemacc::initCheck()
{
    DPRINTF(ProtoMWpipe,"Cycle: %u Memacc : %d filled \n",
            uint64_t(cpu->curCycle()), mwPipe.quantity());
    feedbackCheck();

    if (inputBuffer.touched() == true) pulled = emPipe.pop(&inputBuffer);
    if (stageState() == Avail && !inputBuffer.touched() && 
        internalBuffer.touched() == true)
        begin();

    // Increment the fetchoffset per potential fetch in next cycle, 
    // if begin is not called (as it will do the job otherwise)
    // Problematic

    if (!pushed) pipeEve.schedule();
    memaccEve.schedule(cpu->nextCycle());
}

void 
InstMemacc::begin()
{
    switchTC();
    stageState(Busy);
    memInst = inputBuffer.unload();
    if (memInst->isMemRef())
        DPRINTF(ProtoMemaccStage,"Cycle: %u Memacc: Accessing for %s! at "
                "%#010x \n", uint64_t(cpu->curCycle()),memInst->getName(),
                inputBuffer.dataAddr());
    else {
        DPRINTF(ProtoMemaccStage,"Cycle: %u Memacc: Bypassing for %s! at "
                "%#010x \n", uint64_t(cpu->curCycle()),memInst->getName(),
                inputBuffer.dataAddr());
        memaccComplete();
    }
}

// Utilized in proto.cc
void
InstMemacc::memaccInst(PacketPtr pkt)
{   
    assert(stageState() == Busy);
    stageState(Accessing);

    SimpleThread * trd_ptr = threadCtxt()->thread;
    TheISA::PCState pcState = trd_ptr->pcState();
   
    // received a response from the dcache: complete the load or store
    // instruction
    assert(!pkt->isError());
    assert(cpu->dPortStatus == cpu->DcacheWaitResponse || 
           pkt->req->getFlags().isSet(Request::NO_ACCESS));
    pkt->req->setAccessLatency();

    // updateCycleCounters(BaseCPU::CPU_STATE_ON);

    if (pkt->senderState) {
        SplitFragmentSenderState * send_state =
            dynamic_cast<SplitFragmentSenderState *>(pkt->senderState);
        assert(send_state);
        delete pkt;
        PacketPtr big_pkt = send_state->bigPkt;
        delete send_state;

        SplitMainSenderState * main_send_state =
            dynamic_cast<SplitMainSenderState *>(big_pkt->senderState);
        assert(main_send_state);
        // Record the fact that this packet is no longer outstanding.
        assert(main_send_state->outstanding != 0);
        main_send_state->outstanding--;

        if (main_send_state->outstanding) {
            return;
        } else {
            delete main_send_state;
            big_pkt->senderState = NULL;
            pkt = big_pkt;
        }
    }
    // more going on....
    Fault fault = memInst->completeAcc(pkt, threadCtxt(), cpu->traceData);
    // keep an instruction count
    if (fault == NoFault);
        // countInst();

    else if (cpu->traceData) {
        // If there was a fault, we shouldn't trace this instruction.
        delete cpu->traceData;
        cpu->traceData = NULL;
    }

    delete pkt;
    //fault handling
    DPRINTF(ProtoMemaccInfo,"Memacc: Complete Dcache access\n");     
    memaccComplete();
}

void 
InstMemacc::mwPipePush()
{
    latchWork();
    pushed = mwPipe.push(internalBuffer);
    // mwPipe.reset();
    if (pushed) DPRINTF(ProtoMWpipe,"Cycle: %u Memacc: Pushed data pipe\n",
                        uint64_t(cpu->curCycle()));
    // just for testing purposes only
    // else {pushed = true; internal_buffer.unload();emPipe.reset();}
}

void InstMemacc::latchWork(){

    if (internalBuffer.data()->isLoad())
        emPipe.feedbk(Pipe<EMpipe_buffer>::_doneMemLoad);

    else if (internalBuffer.data()->isStore())
        emPipe.feedbk(Pipe<EMpipe_buffer>::_doneMemStore);
}

#endif