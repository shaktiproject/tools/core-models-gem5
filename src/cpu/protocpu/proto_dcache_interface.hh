#ifndef __CPU_PROTOCPU_PROTO_DCACHE_INTERFACE_HH__
#define __CPU_PROTOCPU_PROTO_DCACHE_INTERFACE_HH__

#include "cpu/protocpu/proto_cc.h"
#include "debug/ProtoMemWrite.hh"
#include "debug/ProtoMemRead.hh"

void
ProtoCPU::Dport::recvTimingSnoopReq(PacketPtr pkt)
{
    for (ThreadID tid = 0; tid < dcpu->numThreads; tid++) {
        if (dcpu->getCpuAddrMonitor(tid)->doMonitor(pkt)) {
            dcpu->wakeup(tid);
        }
    }

    // Making it uniform across all CPUs:
    // (when using caches)
    // The CPUs need to be woken up only on an invalidation packet 
    // (when not using caches)
    // or on an incoming write packet
    // It is not necessary to wake up the processor on all incoming packets
    if (pkt->isInvalidate() || pkt->isWrite()) 
        TheISA::handleLockedSnoop(dcpu->thread, pkt, cacheBlockMask);
}

void
ProtoCPU::Dport::recvFunctionalSnoop(PacketPtr pkt)
{
    for (ThreadID tid = 0; tid < dcpu->numThreads; tid++) {
        if (dcpu->getCpuAddrMonitor(tid)->doMonitor(pkt)) dcpu->wakeup(tid);
    }
}

bool 
ProtoCPU::Dport::recvTimingResp(PacketPtr pkt)
{    
    assert(dcpu->dPortStatus == DcacheWaitResponse);
    DPRINTF(ProtoCPU,"Dport: recvTimingResp \n");
    DtickEve.schedule(pkt, dcpu->clockEdge());
    return true;
}
        
void
ProtoCPU::Dport::recvReqRetry()
{ 
    assert(dcpu->dPortStatus == DcacheRetry);
    dcpu->resendDcacheReq();
    DPRINTF(ProtoCPU,"Dport: recvReqRetry -- \n"); 
}

void
ProtoCPU::sendDcacheReq(const RequestPtr &req, uint8_t *data, uint64_t *res, 
                        bool read)
{
    assert(ProtoCPU::dPortStatus == DcacheIdle);    
    PacketPtr pkt = buildPacket(req, read);
    pkt->dataDynamic<uint8_t>(data);

    if (req->getFlags().isSet(Request::NO_ACCESS)) {
        assert(!dcachePkt);
        pkt->makeResponse();
        completeDcacheAccess(pkt);
    } else if (read) {
        sendDcacheReadReq(pkt);
    } else {
        sendDcacheWriteReq(pkt,res);
    }
}

bool
ProtoCPU::sendDcacheReadReq(PacketPtr pkt)
{    
    const RequestPtr &req = pkt->req;
    // We're about the issues a locked load, so tell the monitor
    // to start caring about this address
    if (pkt->isRead() && pkt->req->isLLSC()) 
        TheISA::handleLockedRead(thread, pkt->req);

    if (req->isLocalAccess()) {
        //Cycles delay = 
        req->localAccessor(thread->getTC(), pkt);
        //new IprEvent(pkt, this, clockEdge(delay));
        //or
        completeDcacheAccess(pkt);
        dPortStatus = DcacheWaitResponse;
        dcachePkt = NULL;
    } else if (!dport.sendTimingReq(pkt)) {
        dPortStatus = DcacheRetry;
        dcachePkt = pkt;
    } else {
        dPortStatus = DcacheWaitResponse;
        // memory system takes ownership of packet
        dcachePkt = NULL;
    }    
  
    // DPRINTF(ProtoCPU,"ALU: Read request to dcache sending mechanism is under test \n");
    return dcachePkt == NULL;
}

bool
ProtoCPU::sendDcacheWriteReq(PacketPtr pkt, uint64_t *res)
{
    const RequestPtr &req = pkt->req;        
    bool do_access = true;  // flag to suppress cache access
    // DPRINTF(ProtoCPU,"ALU: Write request to dcache sending mechanism is under test\n");

    // We're about the issues a locked load, so tell the monitor
    // to start caring about this address
    if (req->isLLSC()) {
        do_access = TheISA::handleLockedWrite(thread, req, dport.cacheBlockMask);
    } else if (req->isCondSwap()) {
            assert(res);
            req->setExtraData(*res);
    }
    if (do_access) {
        if (req->isLocalAccess()) {
           //Cycles delay = 
           req->localAccessor(threadCtxt->thread->getTC(), pkt);
           //new IprEvent(dcache_pkt, this, clockEdge(delay));
           dPortStatus = DcacheWaitResponse;
           dcachePkt = NULL;
        } else if (!dport.sendTimingReq(pkt)) {
           dPortStatus = DcacheRetry;        
           dcachePkt = pkt;
        } else {
           dPortStatus = DcacheWaitResponse;
           // memory system takes ownership of packet
           dcachePkt = NULL;
        }
        threadSnoop(pkt, thread->threadId());

    } else {
        dPortStatus = DcacheWaitResponse;
        completeDcacheAccess(pkt);
    }
    return dcachePkt == NULL;
}                         

void
ProtoCPU::resendDcacheReq()
{
    // we shouldn't get a retry unless we have a packet that we're
    // waiting to transmit
    assert(dcachePkt != NULL);
    assert(dPortStatus == DcacheRetry);    
    PacketPtr tmp = dcachePkt;

    if (tmp->senderState) {
        // This is a packet from a split access.
        SplitFragmentSenderState * send_state =
            dynamic_cast<SplitFragmentSenderState *>(tmp->senderState);
        assert(send_state);
        PacketPtr big_pkt = send_state->bigPkt;

        SplitMainSenderState * main_send_state =
            dynamic_cast<SplitMainSenderState *>(big_pkt->senderState);
        assert(main_send_state);

        if (dport.sendTimingReq(tmp)) {
            // If we were able to send without retrying, record that fact
            // and try sending the other fragment.
            send_state->clearFromParent();
            int other_index = main_send_state->getPendingFragment();
            if (other_index > 0) {
                tmp = main_send_state->fragments[other_index];
                if ((big_pkt->isRead() && sendDcacheReadReq(tmp)) ||
                        (big_pkt->isWrite() && sendDcacheWriteReq(tmp))) {
                    main_send_state->fragments[other_index] = NULL;
                }
            } else {
                dPortStatus = DcacheWaitResponse;
                // memory system takes ownership of packet
                dcachePkt = NULL;
            }
        }
    } else if (dport.sendTimingReq(tmp)) {
        dPortStatus = DcacheWaitResponse;
        // memory system takes ownership of packet
        dcachePkt = NULL;
    }
}

// ISA - Data memory access API

Fault
ProtoCPU::initiateMemRead(Addr addr, unsigned size, Request::Flags flags, 
                          const std::vector<bool>& byteEnable)
{
    // const int asid = 0; // What is this ?
    const Addr pc_addr = threadCtxt->thread->instAddr();
    unsigned block_size = cacheLineSize();
    BaseTLB::Mode mode = BaseTLB::Read;

    DPRINTF(ProtoMemRead,"MemRead: Reading data at %#010lx\n",addr);

    if (traceData) traceData->setMem(addr,size,flags);

    RequestPtr req = std::make_shared<Request>(addr,size,flags,dataMasterId(),
                                               pc_addr,thread->contextId());

    if (!byteEnable.empty()) req->setByteEnable(byteEnable);

    req->taskId(taskId());

    Addr addr_split = roundDown(addr + size -1, block_size);
    // If single data is spread accross two lines of cache, two access is required.
    assert(addr_split <= addr || addr_split - addr < block_size);
 
    //Data spread accross two lines
    if(addr_split > addr)
        fatal("ALU: ProtoCPU does not allow multi-line cache access: Load "
              "address misaligned");   
    else {
        WholeTranslationState *state = 
                new WholeTranslationState(req, new uint8_t[size], NULL, mode);
        DataTranslation<ProtoCPU *> *translation = 
                                new DataTranslation<ProtoCPU *>(this, state);

        ProtoCPU::dPortStatus = DTLBWaitResponse;
        thread->dtb->translateTiming(req, thread->getTC(), translation, mode);
    }
    return NoFault;
}

Fault 
ProtoCPU::writeMem(uint8_t *data, unsigned size, Addr addr, 
                   Request::Flags flags, uint64_t *res, 
                   const std::vector<bool>& byteEnable)
{
    uint8_t *newData = new uint8_t[size];
    const Addr pc = threadCtxt->thread->instAddr();
    unsigned block_size = cacheLineSize();
    BaseTLB::Mode mode = BaseTLB::Write;

    if (data == NULL) {
        assert(flags & Request::STORE_NO_DATA);
        // This must be a cache block cleaning request
        memset(newData, 0, size);
    } else memcpy(newData, data, size);
    

    uint64_t complete_data = 0;
    for(int i = size-1;i>=0;i--){ 
        complete_data = complete_data<<8;
        complete_data += newData[i];
    }
    DPRINTF(ProtoMemWrite,"MemWrite: Writing data = %#lx to %#010lx\n",addr,
            complete_data);
    
    Addr stop_addr = 0x2000c;
    Addr stop_data = 0x20000;
    if (addr == stop_addr &&  complete_data == stop_data) {
        const char end[] ="the binary wrote magic val to secret address !!!\n";
        addr = 0x11310;
        scheduleInstStop(0,0,end);
    }
    
    if (traceData)
        traceData->setMem(addr, size, flags);

    RequestPtr req = std::make_shared<Request>(
        addr, size, flags, dataMasterId(), pc,
        thread->contextId());
    if (!byteEnable.empty()) req->setByteEnable(byteEnable);

    req->taskId(taskId());

    Addr split_addr = roundDown(addr + size - 1, block_size);
    assert(split_addr <= addr || split_addr - addr < block_size);

    if (split_addr > addr) 
        fatal("ALU: ProtoCPU does not allow multi-line cache access: Store "
              "address misaligned \n");
    else {
        WholeTranslationState *state = 
                           new WholeTranslationState(req, newData, res, mode);
        DataTranslation<ProtoCPU *> *translation = 
                                 new DataTranslation<ProtoCPU *>(this, state);
        ProtoCPU::dPortStatus = DTLBWaitResponse;
        thread->dtb->translateTiming(req, thread->getTC(), translation, mode);
    }
    // Translation faults will be returned via finishTranslation()
    return NoFault;
}

Fault
ProtoCPU::initiateMemAMO(Addr addr, unsigned size, Request::Flags flags, 
                               AtomicOpFunctorPtr amo_op)
{
    panic("ALU: Atomic memory operation is undefined at the moment \n");
    Fault fault;
    // const int asid = 0;
    const Addr pc = thread->instAddr();
    unsigned block_size = cacheLineSize();
    BaseTLB::Mode mode = BaseTLB::Write;

    if (traceData)
        traceData->setMem(addr, size, flags);

    RequestPtr req = make_shared<Request>(addr, size, flags,
                            dataMasterId(), pc, thread->contextId(),
                            std::move(amo_op));

    assert(req->hasAtomicOpFunctor());

    req->taskId(taskId());

    Addr split_addr = roundDown(addr + size - 1, block_size);

    // AMO requests that access across a cache line boundary are not
    // allowed since the cache does not guarantee AMO ops to be executed
    // atomically in two cache lines
    // For ISAs such as x86 that requires AMO operations to work on
    // accesses that cross cache-line boundaries, the cache needs to be
    // modified to support locking both cache lines to guarantee the
    // atomicity.
    if (split_addr > addr) 
        panic("ALU: AMO requests should not access across a cache line "
              "boundary\n");

    dPortStatus = DTLBWaitResponse;

    WholeTranslationState *state =
        new WholeTranslationState(req, new uint8_t[size], NULL, mode);
    DataTranslation<ProtoCPU *> *translation
        = new DataTranslation<ProtoCPU *>(this, state);
    thread->dtb->translateTiming(req, thread->getTC(), translation, mode);

    return NoFault;
}       

void 
ProtoCPU::finishTranslation(WholeTranslationState *state)
{    
    assert(dPortStatus == DTLBWaitResponse);
    ProtoCPU::dPortStatus = DcacheIdle;    
    if (state->getFault() != NoFault)
        DPRINTF(ProtoCPU,"ALU: ProtoCPU does not handle dcache translation "
                "fault yet \n");
    else sendDcacheReq(state->mainReq, state->data, state->res, 
                      state->mode == BaseTLB::Read);
}

#endif
