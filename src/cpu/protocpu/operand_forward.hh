#ifndef __CPU_PROTOCPU_OPERAND_FORWARD_HH__
#define __CPU_PROTOCPU_OPERAND_FORWARD_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoOperandForwardInfo.hh"
#include "debug/ProtoOperandForwardStage.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

//Assumes it's being called each time ALU fires up
class OperandForward{

public:
    static int operandForwardStagesCount;

private:
    const Addr noAddr = -1;
    int _operandForwardID;

    ProtoCPU *cpu;
    OFpipe_buffer sourceInst;
    OFpipe_buffer destInst;
    // .data() can never be NULL except for the first time
    Pipe<OFpipe_buffer>& ofPipe;

    bool pulled;

    enum operandForwardStatus 
    {
        Checking_dependency=11,
        Avail,
    };
    operandForwardStatus _stageState;
    
    OFpipe_buffer inputBuffer;

public:
    OperandForward(Pipe<OFpipe_buffer>& of_pipe,ProtoCPU * _cpu):

    cpu(_cpu),
    sourceInst(NULL),
    destInst(NULL),
    ofPipe(of_pipe),
    pulled(true),
    _stageState(Avail),
    inputBuffer(NULL)
    {
       operandForwardStagesCount++;
        _operandForwardID=operandForwardStagesCount;
        DPRINTF(ProtoOperandForwardStage,"OperandForward stage %d activated\n"
                , operandForwardStagesCount);
    }


    bool stall(StaticInstPtr , Addr);
    int operandForwardID();
    void ofPipeCheck();

private:
    bool destRegRetired();
    void operandForwardComplete();

    void stageState(operandForwardStatus f_s){
        _stageState = f_s;
    }
    operandForwardStatus stageState(){
        return _stageState;
    }

};

#endif