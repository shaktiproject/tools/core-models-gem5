from m5.params import *
from m5.objects.BaseCPU import BaseCPU

class ProtoCPU(BaseCPU):
    type = 'ProtoCPU'
    cxx_header = 'cpu/protocpu/proto.hh'

    @classmethod
    def memory_mode(cls):
        return 'timing'

    branchPred = Param.BranchPredictor(NULL, "Branch Predictor")
    mulStages = Param.Int(4, "Number of multiplier stages")
    divStages = Param.Int(64, "Number of divider stages")