#ifndef __CPU_PROTOCPU_INST_TRANSLATE_HH__
#define __CPU_PROTOCPU_INST_TRANSLATE_HH__

#include "cpu/base.hh"

//ITLB access
template <class CPU>
class InstTranslate : public BaseTLB::Translation
{
    enum state{
        ITLBWaitResponse,
        Avail,
    };
    state itState;
    protected:
        CPU *cpu;

    public:
        InstTranslate(CPU *_cpu):itState(Avail),cpu(_cpu)
        {}
    
        void 
        markDelayed()
        {
            assert(itState == Avail);
            itState = ITLBWaitResponse;
        }

        void 
        finish(const Fault &fault, const RequestPtr &req, 
                    ThreadContext *tc, BaseTLB::Mode mode)
        {
            if (fault == NoFault) cpu->iFetch_send_ireq(req, tc);
            else fatal("ProtoCPU: Instruction translation faulted. \n");

            itState = Avail;
        }

};

#endif