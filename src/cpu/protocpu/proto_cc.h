#include "cpu/protocpu/proto.hh"
#include "params/ProtoCPU.hh"
#include "cpu/protocpu/exec_context.hh"

#include "cpu/protocpu/proto_dcache_interface.hh"

#include "cpu/protocpu/inst_translate.hh"
#include "cpu/protocpu/inst_fetch.hh"
#include "cpu/protocpu/inst_decode.hh"
#include "cpu/protocpu/inst_execute.hh"
#include "cpu/protocpu/inst_memacc.hh"
#include "cpu/protocpu/inst_writeback.hh"
#include "cpu/protocpu/operand_forward.hh"

#include "arch/locked_mem.hh"
#include "arch/stacktrace.hh"
#include "arch/utility.hh"
#include "base/cp_annotate.hh"
#include "base/cprintf.hh"
#include "base/inifile.hh"
#include "base/loader/symtab.hh"
#include "base/logging.hh"
#include "base/pollevent.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "config/the_isa.hh"
#include "cpu/base.hh"
#include "cpu/checker/cpu.hh"
#include "cpu/checker/thread_context.hh"
#include "cpu/exetrace.hh"
#include "cpu/pred/bpred_unit.hh"
#include "cpu/profile.hh"
#include "cpu/simple_thread.hh"
#include "cpu/smt.hh"
#include "cpu/static_inst.hh"
#include "cpu/thread_context.hh"

#include "debug/Config.hh"
#include "debug/Drain.hh"
#include "debug/ExecFaulting.hh"
#include "debug/Mwait.hh"
// #include "debug/Decode.hh"
// #include "debug/Fetch.hh"
// #include "debug/Quiesce.hh"
#include "mem/packet.hh"
#include "mem/request.hh"
#include "sim/byteswap.hh"
#include "sim/debug.hh"
#include "sim/faults.hh"
#include "sim/full_system.hh"
#include "sim/sim_events.hh"
#include "sim/sim_object.hh"
#include "sim/stats.hh"
#include "sim/system.hh"
#include "sim/eventq.hh"

using namespace std;
using namespace TheISA;
/*!
    @brief  The namespace TheISA will correspond to the ISA from params
            It contains some of the definitions of regiters and other 
            ISA related attributes under thread object 
    @note   The variable ZeroReg will be reported undefined if this 
            namespace is not used +
 */