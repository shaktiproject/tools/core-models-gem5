#ifndef __CPU_PROTOCPU_PIPE_HH__
#define __CPU_PROTOCPU_PIPE_HH__

#include "cpu/base.hh"

template <typename buffer_type>
class Pipe
{
  protected:
    int bufferSize;
    std::vector<buffer_type> pipeBuffer;
    // void dummy(){}
    // void pushCall(){}
    // void(Pipe<buffer_type>::pushCall)()=dummy;
  public:
    enum feedback{
        _pcInvalid=11,
        _pcValid,
        _none,
        _initMemStore,
        _initMemLoad,
        _doneMemStore,
        _doneMemLoad,
    };
    feedback _feedback;
  
  public:
    Pipe(int buffer_size):bufferSize(buffer_size), _feedback(_none)
    {}

    // Pipe(int buffer_size, void (*_push_call)()):bufferSize(buffer_size), 
    // _feedback(_none),pushCall(_push_call)
    // {}

    bool
    push(buffer_type & internal_buffer)
    {
        if (pipeBuffer.size() < bufferSize) {
            assert(internal_buffer.touched() == false);
            pipeBuffer.push_back(internal_buffer);
            internal_buffer.unload();
            callOnPush();
            return true;
        }
        return false;
    }

    bool
    pop(buffer_type *b)
    {
        if (!pipeBuffer.empty() && b->touched() == true) {
            buffer_type ib = pipeBuffer.front();
            b->load(ib.data(),ib.dataAddr());
            pipeBuffer.erase(pipeBuffer.begin());
            return true;
        }
        // b.reset();// decode will be affected
        return false;
    }
    
    bool
    pop()
    {
        if (!pipeBuffer.empty()) {
            pipeBuffer.erase(pipeBuffer.begin());
            return true;
        }
        // b.reset();// decode will be affected
        return false;
    }

    int
    quantity()
    {
        return pipeBuffer.size();
    }

    bool
    full()
    {
        return pipeBuffer.size()>=bufferSize;
    }

    bool
    empty()
    {
        return pipeBuffer.empty();
    }

    void
    reset()
    {
        pipeBuffer.clear();
    }

    // Override to call upon pushin data
    void callOnPush(){
        // pushCall();
    }

    // Dont use this as this will not reset the feedback flag after read
    feedback
    feedbk()
    {
        return _feedback;
    }
    
    void
    feedbk(feedback f)
    {
        _feedback = f;
    }
    
    bool
    pcValid()
    {
        if (_feedback == _pcValid) {
            _feedback = _none;
            return true;
        }
        return false;
    }
    
    bool
    pcInvalid()
    {
        if (_feedback == _pcInvalid) {
            _feedback = _none;
            return true;
        }
        return false;
    }

    bool
    doneMemStore()
    {
        if (_feedback == _doneMemStore) {
            _feedback = _none;
            return true;
        }
        return false;
    }
    
    bool
    doneMemLoad()
    {
        if (_feedback == _doneMemLoad) {
            _feedback = _none;
            return true;
        }
        return false;
    }
};

#endif