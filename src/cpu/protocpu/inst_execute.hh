#ifndef __CPU_PROTOCPU_INST_EXECUTE_HH__
#define __CPU_PROTOCPU_INST_EXECUTE_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoEMpipe.hh"
#include "debug/ProtoExecuteInfo.hh"
#include "debug/ProtoExecuteStage.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"
#include "cpu/protocpu/operand_forward.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

class InstExecute
{
  public:
    static int executeStagesCount;

  private:
    const Addr noAddr = -1;

    int _executeID;
    ProtoContext *_threadCtxt;
    ProtoContext *_switchTC;
    ProtoCPU *cpu;
    OperandForward *ofu;
    Addr ctrledPC;
    StaticInstPtr aluInst;
    Pipe<DEpipe_buffer>& dePipe;
    Pipe<EMpipe_buffer>& emPipe;

    bool pulled;
    bool pushed;
    bool lockStage;
    bool lockPush;
    bool operandForwardStall;

    enum executeStatus 
    {
        Busy,
        Executing,
        Avail,
    };
    executeStatus _stageState;

    struct PosEdgeEvent: public Event
    {
        InstExecute *ieu;

        PosEdgeEvent(InstExecute *_ieu) : ieu(_ieu) {}
        const char 
        *description() const 
        { 
            return "Execute stage %d's personal post -ve edge scheduler for "
                   "combinatorial circuits"; 
        }

        //Transfer the packet and begin schedule.
        void schedule(Tick t);

        //Event to run on schedule.
        void 
        process()
        {    
            ieu->initCheck();
        }
    };
    PosEdgeEvent executeEve;

    struct ExecEndEvent : public PosEdgeEvent
    {
        ExecEndEvent(InstExecute *_ieu):PosEdgeEvent(_ieu){}
        const char 
        *description() const 
        { 
            return "Execute stage %d's personal execute complete scheduler"; 
        }
        
        void 
        process()
        {
            ieu->executeComplete();
        }
    };
    ExecEndEvent execEndEve;

    struct NegEdgeEvent: public Event
    {
        InstExecute *ieu;

        NegEdgeEvent(InstExecute *_ieu) : ieu(_ieu) {}
        const char 
        *description() const 
        { 
            return "Execute stage %d's personal -ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void 
        process()
        {
            ieu->emPipePush();
        }
    };
    NegEdgeEvent pipeEve;
    
    DEpipe_buffer inputBuffer;

  public:
    EMpipe_buffer internalBuffer;

  public:
    InstExecute(Pipe<DEpipe_buffer>& de_pipe,ProtoCPU * _cpu,
                OperandForward * _ofu, Pipe<EMpipe_buffer>& em_pipe):
    _threadCtxt(NULL),
    _switchTC(NULL),
    cpu(_cpu),
    ofu(_ofu),
    ctrledPC(noAddr),
    aluInst(NULL),
    dePipe(de_pipe),
    emPipe(em_pipe),
    pulled(true),
    pushed(true),
    lockStage(false),
    lockPush(false),
    _stageState(Avail),
    executeEve(this),
    execEndEve(this),
    pipeEve(this),
    inputBuffer(NULL),
    internalBuffer(NULL)
    {
       executeStagesCount++;
        _executeID=executeStagesCount;
        DPRINTF(ProtoExecuteInfo,"Execute stage %d activated\n"
                ,executeStagesCount);
    }

    Addr calculateAddr(ProtoContext *);

    void start();
    void stop();
    void initCheck();
    void feedbackCheck();
    void begin();
    void executeInst();
    void emPipePush();
    int executeID();
    void switchTC(ProtoContext *p){
        _switchTC = p;
    }

  private:
    void 
    aluLock()
    {
        lockStage = true;
    }
    
    void 
    aluUnlock()
    {
        lockStage = false;
    }
    
    bool 
    isAluLocked()
    {
        return lockStage;
    }
    
    void 
    aluPushLock()
    {
        lockPush = true;
    }
    
    void 
    aluPushUnlock()
    {
        lockPush = false;
    }
    
    bool 
    isAluPushLocked()
    {
        return lockPush;
    }
    
    void instDependentCheck();

    void refresh(){}

    void 
    executeComplete()
    {
        assert(stageState()== Executing);
        stageState(Avail);
        pipeEve.schedule();
    }

    void 
    switchTC()
    {
        assert(stageState() == Avail);
        threadCtxt(_switchTC);
    }
    
    void 
    stageState(executeStatus f_s)
    {
        _stageState = f_s;
    }
    
    executeStatus 
    stageState()
    {
        return _stageState;
    }
    
    void 
    threadCtxtRst(void)
    {
        _threadCtxt = NULL;
    }
    
    void 
    threadCtxt(ProtoContext *t)
    {
        _threadCtxt = t;
    }
    
    ProtoContext 
    * threadCtxt()
    {
        return _threadCtxt;
    }
};
#endif