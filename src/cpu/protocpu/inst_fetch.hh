/*!
    @file inst_fetch.hh
    @date Wed March 11 2020
    
    @copyright Copyright (c) 2020 

    @brief  Contains the definition of methods of instruction fetch class
 */
#ifndef __CPU_PROTOCPU_INST_FETCH_HH__
#define __CPU_PROTOCPU_INST_FETCH_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoFDpipe.hh"
#include "debug/ProtoFetchInfo.hh"
#include "debug/ProtoFetchStage.hh"
#include "cpu/protocpu/iport.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

class InstFetch
{
  public:
    static int fetchStagesCount;

  private:

    const MachInst noInst = -1;
    const Addr noAddr = -1;

    int _fetchID;
    ProtoContext *_threadCtxt;
    ProtoContext *_switchTC;
    ProtoCPU *cpu;
    PacketPtr icachePkt;
    PacketPtr icachePktBlocked;
    PacketPtr icachePktReceived;
    MachInst inst;
    Addr fetchPC;
    Iport<ProtoCPU> &iport;
    Pipe<FDpipe_buffer>& fdPipe;
    bool pushed;
    bool lockStage;
    enum fetchStatus 
    {
        Busy,
        Try_send,
        Try_resend,
        Waiting_for_valid_resp,
        Avail,
    };
    fetchStatus _stageState;

    struct PosEdgeEvent: public Event
    {
        InstFetch *ifu;
        PosEdgeEvent(InstFetch *_ifu) : ifu(_ifu) {}
        
        const char 
        *description() const 
        { 
            return "Fetch stage %d's personal +ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule(Tick);

        //Event to run on schedule.
        void 
        process()
        {
            ifu->initCheck();
        }
    };
    PosEdgeEvent fetchEve;

    struct NegEdgeEvent: public Event
    {
        InstFetch *ifu;
        NegEdgeEvent(InstFetch *_ifu) : ifu(_ifu) {}
        const char 
        *description() const 
        { 
            return "Fetch stage %d's personal -ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void 
        process()
        {
            ifu->fdPipePush();
        }
    };
    NegEdgeEvent pipeEve;
    
  public:
    FDpipe_buffer internalBuffer;

  public:
    InstFetch(ProtoCPU * _cpu, Iport<ProtoCPU> & _iport, 
              Pipe<FDpipe_buffer>& fd_pipe):
    _threadCtxt(NULL),
    _switchTC(NULL),
    cpu(_cpu),
    icachePkt(NULL),
    icachePktBlocked(NULL),
    inst(noInst),
    fetchPC(noAddr),
    iport(_iport),
    fdPipe(fd_pipe),
    pushed(true),
    lockStage(false),
    _stageState(Avail),
    fetchEve(this),
    pipeEve(this),
    internalBuffer(noInst)
    {
        fetchStagesCount++;
        _fetchID=fetchStagesCount;
        DPRINTF(ProtoFetchStage,"Fetch stage %d activated\n",fetchStagesCount);
    }

    Addr calculateAddr(ProtoContext *);

    void start();    
    void stop();
    void initCheck();
    void feedbackCheck();
    RequestPtr begin();
    void translateAddr(RequestPtr );
    void sendReq(const RequestPtr &, ThreadContext *);
    FDpipe_buffer &recvInst(PacketPtr );
    void resendReq();
    int fetchID();
    void fdPipePush();
    
    void 
    switchTC(ProtoContext *p)
    {
        _switchTC = p;
    }

  private:
    void
    fetchLock()
    {
        lockStage = true;
    }
    
    void
    fetchUnlock()
    {
        lockStage = false;
    }
    
    bool
    isFetchLocked()
    {
        return lockStage;
    }

    void
    switchTC()
    {
        assert(stageState() == Avail);
        threadCtxt(_switchTC);
    }

    void
    stageState(fetchStatus f_s)
    {
        _stageState = f_s;
    }
    
    fetchStatus
    stageState()
    {
        return _stageState;
    }
    
    void
    threadCtxtRst(void)
    {
        _threadCtxt = NULL;
    }
    
    void 
    threadCtxt(ProtoContext *t)
    {
        _threadCtxt = t;
    }
    
    ProtoContext *
    threadCtxt()
    {
        return _threadCtxt;
    }
};



#endif