#ifndef __CPU_PROTOCPU_INST_MEMACC_HH__
#define __CPU_PROTOCPU_INST_MEMACC_HH__

#include "cpu/base.hh"
#include "cpu/simple_thread.hh"
#include "debug/ProtoMWpipe.hh"
#include "debug/ProtoMemaccInfo.hh"
#include "debug/ProtoMemaccStage.hh"
#include "cpu/protocpu/stage_buffer.hh"
#include "cpu/protocpu/pipe.hh"

using namespace TheISA;
class ProtoCPU;
class ProtoContext;

class InstMemacc{

public:
    static int memaccStagesCount;

private:
    const Addr noAddr = -1;

    int _memaccID;
    ProtoContext *_threadCtxt;
    ProtoContext *_switchTC;
    ProtoCPU *cpu;
    StaticInstPtr memInst;
    Pipe<EMpipe_buffer>& emPipe;
    Pipe<MWpipe_buffer>& mwPipe;

    bool pulled;
    bool pushed;

    enum memaccStatus 
    {
        Busy,
        Accessing,
        Avail,
    };
    memaccStatus _stageState;

    struct PosEdgeEvent: public Event
    {
        InstMemacc *imu;
        PosEdgeEvent(InstMemacc *_imu) : imu(_imu) {}
        
        const char 
        *description() const 
        { 
            return "Memacc stage %d's personal post -ve edge scheduler for"
                   " combinatorial circuits"; 
        }

        //Transfer the packet and begin schedule.
        void schedule(Tick t);

        //Event to run on schedule.
        void 
        process()
        {
            imu->initCheck();
        }
    };
    PosEdgeEvent memaccEve;

    struct ExecEndEvent : public PosEdgeEvent
    {
        ExecEndEvent(InstMemacc *_imu):PosEdgeEvent(_imu){}
        
        const char 
        *description() const 
        { 
            return "Memacc stage %d's personal memacc complete scheduler"; 
        }
        
        void 
        process()
        {
            imu->memaccComplete();
        }
    };
    ExecEndEvent execEndEve;

    struct NegEdgeEvent: public Event
    {
        InstMemacc *imu;
        NegEdgeEvent(InstMemacc *_imu) : imu(_imu) {}

        const char 
        *description() const 
        { 
            return "Memacc stage %d's personal -ve edge scheduler"; 
        }

        //Transfer the packet and begin schedule.
        void schedule();

        //Event to run on schedule.
        void 
        process()
        {
            imu->mwPipePush();
        }
    };
    NegEdgeEvent pipeEve;
    
    EMpipe_buffer inputBuffer;

public:
    MWpipe_buffer internalBuffer;

public:
    InstMemacc(Pipe<EMpipe_buffer>& em_pipe,ProtoCPU * _cpu, 
               Pipe<MWpipe_buffer>& mw_pipe):
    _threadCtxt(NULL),
    _switchTC(NULL),
    cpu(_cpu),
    memInst(NULL),
    emPipe(em_pipe),
    mwPipe(mw_pipe),
    pulled(true),
    pushed(true),
    _stageState(Avail),
    memaccEve(this),
    execEndEve(this),
    pipeEve(this),
    inputBuffer(NULL),
    internalBuffer(NULL)
    {
       memaccStagesCount++;
        _memaccID=memaccStagesCount;
        DPRINTF(ProtoMemaccStage,"Memacc stage %d activated\n",
                memaccStagesCount);
    }

    Addr calculateAddr(ProtoContext *);

    void start();
    void stop();
    void initCheck();
    void memaccInst(PacketPtr);
    int memaccID();
    void switchTC(ProtoContext *p){
        _switchTC = p;
    }

private:
    void feedbackCheck();
    void begin();
    void mwPipePush();
    void latchWork();
    

    void memaccComplete(){
        if(memInst->isMemRef()){ assert(stageState()== Accessing);}
        else{ assert(stageState()== Busy);}
        stageState(Avail);
        internalBuffer.load(memInst,inputBuffer.dataAddr());
        memInst=NULL;
        pipeEve.schedule();
    }

    void switchTC(){
        assert(stageState() == Avail);
        threadCtxt(_switchTC);
    }
    void stageState(memaccStatus f_s){
        _stageState = f_s;
    }
    memaccStatus stageState(){
        return _stageState;
    }
    void threadCtxtRst(void){
        _threadCtxt = NULL;
    }
    void threadCtxt(ProtoContext *t){
        _threadCtxt = t;
    }
    ProtoContext * threadCtxt(){
        return _threadCtxt;
    }

    class SplitMainSenderState : public Packet::SenderState
    {
      public:
        int outstanding;
        PacketPtr fragments[2];

        int
        getPendingFragment()
        {
            if (fragments[0]) {
                return 0;
            } else if (fragments[1]) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    class SplitFragmentSenderState : public Packet::SenderState
    {
      public:
        SplitFragmentSenderState(PacketPtr _bigPkt, int _index) :
            bigPkt(_bigPkt), index(_index)
        {}
        PacketPtr bigPkt;
        int index;

        void
        clearFromParent()
        {
            SplitMainSenderState * main_send_state =
                dynamic_cast<SplitMainSenderState *>(bigPkt->senderState);
            main_send_state->fragments[index] = NULL;
        }
    };
};



#endif