.. ProtoCPU documentation master file, created by
   sphinx-quickstart on Thu Nov 28 15:11:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ProtoCPU's documentation!
====================================

ProtoCPU is a software CPU model under development to mirror 
`c-class <https://shakti.org.in/cclass.html>`_ processor in gem5 simulator with 
cycle accuracy. The model currently supports single threaded riscv baremetal 
full system (FS) emulation mode simulations in 
`gem5 <http://gem5.org/Main_Page>`_ without trap/fault handling. It is coded in
C++ and Python.

Current model is compatible with  v20.0.0.1 of gem5 simulator. The model is 
provided with a simple UART device that's currently capable of transmitting 
data from the CPU.

.. toctree::
   :maxdepth: 1

   why_gem5
   Func_models_gem5
   protocpu
   simng_model

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
