Why model CPU in gem5 ?
=======================

| GEM5 provides the ability to develop a modular CPU model which can support 
plug and play style functional models for cache, memory controllers, GPU, 
Branch predictors etc... which are readily available with the gem5 software 
ecosystem.

| Being a opensource project, it has garnered a lot of constructive activities 
in it's codebase for the past several years. Besides, the support for RISCV in 
gem5 has gained a lot of traction  `recently <https://www.mail-archive.com/search?a=1&l=gem5-dev%40gem5.org&haswords=riscv&x=0&y=0&from=&subject=&datewithin=1d&date=&notwords=&o=newest>`_.
