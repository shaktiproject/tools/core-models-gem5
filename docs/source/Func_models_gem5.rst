Functional models available in gem5
===================================

Note: The gem5 team is conducting a major revamp of the gem5 environment. Some
of the information and links provided here may be obsolete.

Source: http://gem5.org/Main_Page

Apart from the ability to develop our own HW models (SimObjects), the following 
models are readily available for use.

1. Four detailed CPU models (`unlike KVM and TraceCPU <http://gem5.org/TraceCPU>`_)

    * `Atomic Simple <http://gem5.org/SimpleCPU#AtomicSimpleCPU>`_ (Timing details: 1 CPI with atomic cache latency)
    * `Timing Simple <http://gem5.org/SimpleCPU#TimingSimpleCPU>`_ (Timing details: 1 CPI with timing cache latency)
    * Minor (4 stage)
    * O3C (Our of order)

2. GPU

    * Fully integrated GPU (Can execute ISA)
    * NoMali (Just the interface, nothing is rendered, no display output)

3. `Branch Predictor <http://www.gem5.org/docs/html/classBPredUnit.html)>`_

    * BiMode
    * Local
    * LTAGE
    * Tournament

4. Event driven Memory systems

    * Configurable cache model
    * Snoop filters
    * Crossbars
    * Configurable DRAM controller

4. Multi-system simulation (Server - client)

5. `Supported ISA <http://gem5.org/Supported_Architectures>`_ with full system simulation & syscall emulation .

    * ARM (Linux bootable)
    * x86 (Standard PC)
    * SPARC (Solaris bootable)
    * ALPHA (Linux bootable)

6. `Supported ISA <http://gem5.org/Supported_Architectures>`_ with only syscall emulation

    * RISCV
    * PowerPC
    * MIPS

9. Power and Energy modelling (DVFS...)

8. ISA modelling support

    * http://gem5.org/How_to_implement_an_ISA
    * http://www.gem5.org/ISA_description_system

.. note::
   * Peripherals such as Internet network adapters, floating point units are available at gem5/ext/...
   * Example configuration scripts in python is available at gem5/configs/...