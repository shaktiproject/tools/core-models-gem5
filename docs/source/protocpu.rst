ProtoCPU
========

Characteristic of the CPU model:

* Typical Five stage pipeline
* Multi line I-cache access is allowed
* Multi-line D-cache access is not allowed (Store/Load misaligned)
* Decode stage stores the half word from previous fetch and recombines it with half word in the next fetch incase of an uncompressed instruction.
* Address calculation and dcache request for memory referencing instruction takes place at ALU stage
* Operand forward unit detects RAW dependencies
* Dcache response is processed in memory access stage
* Write back stage marks the end of an instruction and register update by that instruction
* Fault/trap handling support yet to be implemented

Note: RAW dependency is detected by operand forward unit and resolved by 
pipeline stall.

The cpu model ProtoCPU uses:

* Masterport class template for D-cache and I-cache interface
* Execcontext class for maintaining statistical details and ISA interface
* Simplethread class aids in maintaining the hardware context (GPRs etc.) details of the CPU.