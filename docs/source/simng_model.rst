Simulating the model
====================
The model can be simulated with the gem5 version 20.0.0.1 using the following 
steps:



Debug flags
-----------
DebugFlag('ProtoCPU')

DebugFlag('ProtoFetchInfo')
DebugFlag('ProtoFetchStage')
DebugFlag('ProtoFDpipe')
CompoundFlag('ProtoFetch', [
    'ProtoFetchInfo', 'ProtoFetchStage', 'ProtoFDpipe'])

DebugFlag('ProtoDecodeInfo')
DebugFlag('ProtoDecodeStage')
DebugFlag('ProtoDEpipe')
CompoundFlag('ProtoDecode', [
    'ProtoDecodeInfo', 'ProtoDecodeStage', 'ProtoDEpipe'])

DebugFlag('ProtoExecuteInfo')
DebugFlag('ProtoExecuteStage')
DebugFlag('ProtoEMpipe')
CompoundFlag('ProtoExecute', [
    'ProtoExecuteInfo', 'ProtoExecuteStage', 'ProtoEMpipe'])
    
DebugFlag('ProtoMemaccInfo')
DebugFlag('ProtoMemaccStage')
DebugFlag('ProtoMWpipe')
CompoundFlag('ProtoMemacc', [
    'ProtoMemaccInfo', 'ProtoMemaccStage', 'ProtoMWpipe'])

DebugFlag('ProtoWritebackInfo')
DebugFlag('ProtoWritebackStage')
CompoundFlag('ProtoWriteback', [
    'ProtoWritebackInfo', 'ProtoWritebackStage'])

DebugFlag('ProtoOperandForwardInfo')
DebugFlag('ProtoOperandForwardStage')
CompoundFlag('ProtoOperandForward', [
    'ProtoOperandForwardInfo','ProtoOperandForwardStage'])

DebugFlag('ProtoIport')
DebugFlag('ProtoMemWrite')
DebugFlag('ProtoMemRead')

CompoundFlag('ProtoStages', [
    'ProtoFetchStage', 'ProtoDecodeStage', 'ProtoExecuteStage',
    'ProtoMemaccStage', 'ProtoWritebackStage', 'ProtoOperandForwardStage',
    'ProtoIport'])

CompoundFlag('ProtoPipes', [
    'ProtoFDpipe', 'ProtoDEpipe', 'ProtoEMpipe', 'ProtoMWpipe'])