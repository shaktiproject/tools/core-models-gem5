#!/bin/python3
import os
import sys
import subprocess
import time
gem5_version = 'v20.0.0.1'
print("The model requires gem5 "+gem5_version)

gem5_url = 'https://gem5.googlesource.com/public/gem5'
current_dir = os.getcwd()

# Check for the existence of gem5 simulator
gem5_path_exists = False

gem5_path = sys.argv[1]
gem5_path = os.path.abspath(gem5_path)
if os.path.isdir(gem5_path):
    print("Found existing gem5 repository at " + gem5_path)

    # os.chdir(current_dir)

    cmd = 'cp -r '+current_dir+'/configs '+current_dir+'/riscv;'
    cmd += 'cp -r '+current_dir+'/riscv '+gem5_path+'/configs/example/;'
    cmd += 'rm -r '+current_dir+'/riscv'
    subprocess.call(cmd,stdout=subprocess.DEVNULL,
                                    stderr=subprocess.DEVNULL,shell=True)

    os.chdir(gem5_path)
    print('Building gem5')
    cmd = 'scons' + ' EXTRAS=\"{0}/src\"'.format(current_dir) + ' '
    cmd += gem5_path + '/build/RISCV/gem5.opt -j$(nproc)'
    ret = subprocess.call(cmd,stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL,shell=True)
    # ret = subprocess.call('', stdout=subprocess.DEVNULL,
    #                       stderr=subprocess.DEVNULL, shell=True)

    if ret != 0:
        print("Error building gem5 !")
        sys.exit(1)

    print("Done updating the simulator")
    cmd = '{0}/build/RISCV/gem5.opt -r --stdout=cool '
    cmd += '--debug-flags=ProtoUART,ProtoStages '
    cmd += '{0}/configs/example/riscv/baremetal_l2_uart.py '
    cmd += '--exe={1}/test/hello.riscv '
    gcmd = cmd.format(gem5_path,current_dir)

    cmd = "grep \'TX\' m5out/cool"
    print("\nPrinting the UART output ({}):".format(cmd))
    ret = subprocess.call(gcmd, stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL, shell=True)
    if ret != 0:
        print("Error simulating with gem5 !")
        sys.exit(1)
    ret = subprocess.call(cmd,shell=True)

    print("\nTo run this simulation again, use the following command:")
    print("\"",end='')
    print(gcmd,end='')
    print("\"\n")
    print("To see the full simulation result:")
    print("\"nano m5out/cool\"\n")
    p = "To run other riscv binaries with the simulator, pass the path of the "
    p += 'binary after \"--exe=\"'
    print(p)